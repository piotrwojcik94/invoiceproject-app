import {KPiR} from "../../entities/invoice/KPiR.model";

export class KpirColumns {
  static header = {
    row_1: {
      col_1: {rowSpan: 2, text: 'Lp.', style: 'headerStyle'},
      col_2: {rowSpan: 2, text: 'Data\nzdarzenia\ngospo-\ndarczego', style: 'headerStyle'},
      col_3: {rowSpan: 2, text: 'Nr\ndowodu\nksię-\ngowego', style: 'headerStyle'},
      col_4: {colSpan: 2, text: 'Kontrahent', style: 'headerStyle'},
      col_5: {},
      col_6: {rowSpan: 2, text: 'Opis\nzdarzenia\ngospo-\ndarczego', style: 'headerStyle'},
      col_7: {colSpan: 3, text: 'Przychód', style: 'headerStyle'},
      col_8: {},
      col_9: {},
      col_10: {rowSpan: 2, text: 'Zakup\ntowarów\nhandlowych\ni\nmateriałów\nwg cen\nzakupu', style: 'headerStyle'},
      col_11: {rowSpan: 2, text: 'Koszty\nuboczne\nzakupu', style: 'headerStyle'},
      col_12: {colSpan: 4, text: 'Wydatki (koszty)', style: 'headerStyle'},
      col_13: {},
      col_14: {},
      col_15: {},
      col_16: {rowSpan: 2, text: 'Uwagi', style: 'headerStyle'},
    },
    row_2: {
      col_1: {},
      col_2: {},
      col_3: {},
      col_4: {text: 'Imię\ni nazwisko\n(firma)', style: 'headerStyle'},
      col_5: {text: 'Adres', style: 'headerStyle'},
      col_6: {},
      col_7: {text: 'Wartość\nsprzedanych\ntowarów i\nusług', style: 'headerStyle'},
      col_8: {text: 'Pozostałe\nprzychody', style: 'headerStyle'},
      col_9: {text: 'Razem\nprzychód\n(7 + 8)', style: 'headerStyle'},
      col_10: {},
      col_11: {},
      col_12: {text: 'Wynagrodzenia\nw gotówce i w\nnaturze', style: 'headerStyle'},
      col_13: {text: 'Pozostałe\nwydatki', style: 'headerStyle'},
      col_14: {text: 'Razem\nwydatki\n(12+13)', style: 'headerStyle'},
      col_15: {text: '', style: 'headerStyle'},
      col_16: {}
    },
    row_3: {
      col_1: {text: '1', style: 'headerStyle'},
      col_2: {text: '2', style: 'headerStyle'},
      col_3: {text: '3', style: 'headerStyle'},
      col_4: {text: '4', style: 'headerStyle'},
      col_5: {text: '5', style: 'headerStyle'},
      col_6: {text: '6', style: 'headerStyle'},
      col_7: {text: '7', style: 'headerStyle'},
      col_8: {text: '8', style: 'headerStyle'},
      col_9: {text: '9', style: 'headerStyle'},
      col_10: {text: '10', style: 'headerStyle'},
      col_11: {text: '11', style: 'headerStyle'},
      col_12: {text: '12', style: 'headerStyle'},
      col_13: {text: '13', style: 'headerStyle'},
      col_14: {text: '14', style: 'headerStyle'},
      col_15: {text: '15', style: 'headerStyle'},
      col_16: {text: '16', style: 'headerStyle'},
    }
  };
  static getFooter(kpir: KPiR) {
    return {
      row_1: {
        col_1: {text: '', border: [false, false, true, false], alignment: 'center', colSpan: 4},
        col_2: {},
        col_3: {},
        col_4: {},
        col_5: {colSpan: 2, text: 'Suma strony', style: 'headerStyle'},
        col_6: {},
        col_7: {
          text: kpir.incomeAndExpensesCurrentPage.valueGoodsAndServicesSold.toFixed(2),
          style: 'rowStyle'
        },
        col_8: {
          text: kpir.incomeAndExpensesCurrentPage.otherIncome.toFixed(2),
          style: 'rowStyle'
        },
        col_9: {
          text: kpir.incomeAndExpensesCurrentPage.totalIncome.toFixed(2),
          style: 'rowStyle'
        },
        col_10: {text: '-', style: 'rowStyle'},
        col_11: {text: '-', style: 'rowStyle'},
        col_12: {text: kpir.incomeAndExpensesCurrentPage.payInCash.toFixed(2), style: 'rowStyle'},
        col_13: {text: kpir.incomeAndExpensesCurrentPage.otherExpenses.toFixed(2), style: 'rowStyle'},
        col_14: {text: kpir.incomeAndExpensesCurrentPage.totalExpenses.toFixed(2), style: 'rowStyle'},
        col_15: {text: '-', style: 'rowStyle'},
        col_16: {text: '-', style: 'rowStyle'},
      },
      row_2: {
        col_1: {text: '', border: [false, false, true, false], alignment: 'center', colSpan: 4},
        col_2: {},
        col_3: {},
        col_4: {},
        col_5: {colSpan: 2, text: 'Przeniesienie z poprzedniej strony', style: 'headerStyle'},
        col_6: {},
        col_7: {
          text: kpir.incomeAndExpensesPreviousPage.valueGoodsAndServicesSold.toFixed(2),
          style: 'rowStyle'
        },
        col_8: {
          text: kpir.incomeAndExpensesPreviousPage.otherIncome.toFixed(2),
          style: 'rowStyle'
        },
        col_9: {
          text: kpir.incomeAndExpensesPreviousPage.totalIncome.toFixed(2),
          style: 'rowStyle'
        },
        col_10: {text: '-', style: 'rowStyle'},
        col_11: {text: '-', style: 'rowStyle'},
        col_12: {text: kpir.incomeAndExpensesPreviousPage.payInCash.toFixed(2), style: 'rowStyle'},
        col_13: {text: kpir.incomeAndExpensesPreviousPage.otherExpenses.toFixed(2), style: 'rowStyle'},
        col_14: {text: kpir.incomeAndExpensesPreviousPage.totalExpenses.toFixed(2), style: 'rowStyle'},
        col_15: {text: '-', style: 'rowStyle'},
        col_16: {text: '-', style: 'rowStyle'},
      },
      row_3: {
        col_1: {text: '', border: [false, false, true, false], alignment: 'center', colSpan: 4},
        col_2: {},
        col_3: {},
        col_4: {},
        col_5: {colSpan: 2, text: 'Razem od początku roku', style: 'headerStyle'},
        col_6: {},
        col_7: {
          text: kpir.incomeAndExpensesYear.valueGoodsAndServicesSold.toFixed(2),
          style: 'rowStyle'
        },
        col_8: {
          text: kpir.incomeAndExpensesYear.otherIncome.toFixed(2),
          style: 'rowStyle'
        },
        col_9: {
          text: kpir.incomeAndExpensesYear.totalIncome.toFixed(2),
          style: 'rowStyle'
        },
        col_10: {text: '-', style: 'rowStyle'},
        col_11: {text: '-', style: 'rowStyle'},
        col_12: {text: kpir.incomeAndExpensesYear.payInCash.toFixed(2), style: 'rowStyle'},
        col_13: {text: kpir.incomeAndExpensesYear.otherExpenses.toFixed(2), style: 'rowStyle'},
        col_14: {text: kpir.incomeAndExpensesYear.totalExpenses.toFixed(2), style: 'rowStyle'},
        col_15: {text: '-', style: 'rowStyle'},
        col_16: {text: '-', style: 'rowStyle'},
      }
    };
  }
  static getData(body: any, kpir: KPiR) {
    let i = 1;
    for (let invoice in kpir.invoices) {
      let inv = kpir.invoices[invoice];
      let row = [];
      row.push({text: i, style: 'countCell'});
      row.push({text: inv.posting, style: 'data1Cell'});
      row.push({text: inv.number, style: 'data1Cell'});
      row.push({text: inv.contractor.name, style: 'data3Cell'});
      row.push({text: inv.contractor.getContractorAddress(), style: 'data3Cell'});
      row.push({text: inv.invoiceIncomeAndExpenses.description, style: 'data3Cell'});
      row.push({text: inv.invoiceIncomeAndExpenses.valueGoodsAndServicesSold.toFixed(2), style: 'data2Cell'});
      row.push({text: (0).toFixed(2), style: 'data2Cell'});
      row.push({text: inv.invoiceIncomeAndExpenses.totalIncome.toFixed(2), style: 'data2Cell'});
      row.push({text: (0).toFixed(2), style: 'data2Cell'});
      row.push({text: (0).toFixed(2), style: 'data2Cell'});
      row.push({text: inv.invoiceIncomeAndExpenses.otherExpenses.toFixed(2), style: 'data2Cell'});
      row.push({text: (0).toFixed(2), style: 'data2Cell'});
      row.push({text: inv.invoiceIncomeAndExpenses.totalExpenses.toFixed(2), style: 'data2Cell'});
      row.push({text: '', style: 'data2Cell'});
      row.push({text: inv.invoiceIncomeAndExpenses.comments, style: 'data3Cell'});
      body.push(row);
      i++;
    }
  }
  static setDataOnPDF(body: any, data: any) {
    for (let key in data){
      if(data.hasOwnProperty(key)) {
        let header = data[key];
        let row = [];
        for (let key in header) {
          if(header.hasOwnProperty(key)) {
            row.push(header[key]);
          }
        }
        body.push(row);
      }
    }
  }
}
