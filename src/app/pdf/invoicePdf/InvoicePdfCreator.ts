import {InvoicePdfHelper} from "./InvoicePdfHelper";
import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoicePdf} from "./InvoicePdf";

export class InvoicePdfCreator {
  public header: [] = [];
  public contractors: [] = [];
  public invoiceItems: [] = [];
  public payments: [] = [];
  public signature: [] = [];
  private helper: InvoicePdfHelper;

  constructor(invoice: Invoice) {
    this.helper = new InvoicePdfHelper(invoice);
  }

  setHeader() {
    this.helper.setHeader(this.header);
    return this;
  }

  setContractors() {
    this.helper.setContractors(this.contractors);
    return this;
  }

  setInvoiceItems() {
    this.helper.setInvoiceItems(this.invoiceItems);
    return this;
  }

  setPaymentData() {
    this.helper.setPaymentData(this.payments);
    return this;
  }

  setSignature() {
    this.helper.setSignature(this.signature);
    return this;
  }

  create() {
    let data = [];
    data.push({columns: this.header});
    data.push({margin: [0,10,0,0], columns: this.contractors});
    data.push(this.invoiceItems);
    data.push(this.payments);
    data.push(this.signature);
    return data;
  }
}
