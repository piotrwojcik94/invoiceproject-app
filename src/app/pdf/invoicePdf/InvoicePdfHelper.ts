import {Invoice} from "../../entities/invoice/Invoice.model";
import {Contractor} from "../../entities/invoice/Contractor.model";
import {AppSettings} from "../../app-settings";
import {CalculateDateComponent} from "../../tools/calculate-date.component";
import {ConvertNumberToWord} from "../../tools/ConvertNumberToWord";

export class InvoicePdfHelper {

  invoice: Invoice;

  constructor(invoice: Invoice) {
    this.invoice = invoice;
  }

  setHeader(header: any) {
    header.push(this.getStampData());
    header.push(this.getHeaderData());
    header.push(this.getDateData());
  }

  getStampData() {
    return {
      margin: [75,0,0,0],
      alignment: 'center',
      table: {
        heights: 30,
        widths: ['80%'],
        body: [
          [{text: 'pieczęć wystawcy', style: 'stamp'}]
        ]
      }
    }
  }

  getHeaderData() {
    return {
      alignment: 'center',
      table: {
        heights: 10,
        widths: ['80%'],
        body: [
          [
            {
              table: {
                widths: ['100%'],
                body: [
                  ['FAKTURA VAT']
                ]
              }
            }
          ],
          [{
            margin: [0,8,0,0],
            table: {
              widths: ['100%'],
              body: [
                [{text: 'NR: '+this.invoice.number}]
              ]
            }
          }],
          [{
            margin: [0,8,0,0],
            table: {
              widths: ['100%'],
              body: [
                ['ORYGINAŁ / KOPIA']
              ]
            }
          }],
        ]
      }, layout: 'noBorders'
    }
  }

  getDateData() {
    return {
      margin: [0,0,25,0],
      table: {
        heights: 10,
        widths: ['80%'],
        body: [
          [
            {
              table: {
                widths: ['50%', '50%'],
                body: [
                  [{text: 'Data\nwystawienia:', style: 'small', border: [true, true, false, true]}, {text:CalculateDateComponent.getFormattedDate(this.invoice.issue), style: 'small2', border: [false, true, true, true]}]
                ]
              }
            }
          ],
          [{
            margin: [0,5,0,0],
            table: {
              widths: ['50%','50%'],
              body: [
                [{text: 'Data\nsprzedaży:', style: 'small', border: [true, true, false, true]}, {text:CalculateDateComponent.getFormattedDate(this.invoice.delivery), style: 'small2', border: [false, true, true, true]}]
              ]
            }
          }],
        ]
      }, layout: 'noBorders'
    }
  }

  setContractors(contractors: any) {
    contractors.push(this.getSellerData());
    contractors.push(this.getCustomerData());
  }

  getSellerData() {
    let contractor = new Contractor(JSON.parse(<string>localStorage.getItem(AppSettings.CONTRACTOR)));
    return {
      margin: [75,0,0,0],
      table: {
        widths: ['40%','40%'],
        body: [
          [{colSpan: 2, text: 'SPRZEDAWCA', style: 'contractor'}, {}],
          [{colSpan: 2, text: contractor.name, style: 'contractor'}, {}],
          [{colSpan: 2, text: contractor.getContractorAddress(), style: 'contractor'}, {}],
          [{text: 'NIP: ' + contractor.nip, style: 'contractor'},{text: 'REGON: ' + contractor.regon, style: 'contractor'}]
        ]
      }
    };
  }

  getCustomerData() {
    let contractor = new Contractor(this.invoice.contractor);
    return {
      margin: [50,0,15,0],
      table: {
        widths: ['40%', '40%'],
        body: [
          [{colSpan: 2, text: 'NABYWCA', style: 'contractor'}, {}],
          [{colSpan: 2, text: contractor.name, style: 'contractor'}, {}],
          [{colSpan: 2, text: contractor.getContractorAddress(), style: 'contractor'}, {}],
          [{text: 'NIP: '+contractor.nip, style: 'contractor'}, {text: 'REGON: ', style: 'contractor'}]
        ]
      }
    };
  }

  setInvoiceItems(invoiceItems: any) {
    invoiceItems.push(this.getInvoiceItems());
    invoiceItems.push(this.getInfoAboutContract());
    invoiceItems.push(this.getVATRate());
  }

  getInvoiceItems() {
    let body = [];
    body = this.getInvoiceItemsData();
    return {
      margin: [50,10,0,0],
      table: {
        widths: [15, 88, 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
        body: body
      }
    }
  }

  getInvoiceItemsData() {
    let data = [];
    let i = 1;
    let row = [];
    row.push({text: 'Lp.', style: 'contractor'});
    row.push({text: 'Nazwa towaru lub usługi', style: 'contractor'});
    row.push({text: 'j.m.', style: 'contractor'});
    row.push({text: 'Ilość', style: 'contractor'});
    row.push({text: 'Cena jednostkowa\nbez podatku', style: 'contractor'});
    row.push({text: 'Wartość\ntowaru/usługi\nbezpodatku', style: 'contractor'});
    row.push({text: 'VAT%', style: 'contractor'});
    row.push({text: 'Kwota podatku', style: 'contractor'});
    row.push({text: 'Wartość\ntowaru/usługi wraz\nz podatkiem', style: 'contractor'});
    data.push(row);
    for(let item of this.invoice.invoiceItems) {
      row = [];
      row.push({text: i, style: 'size8', alignment: 'center'});
      row.push({text: item.description, style: 'size8'});
      row.push({text: item.unit, style: 'size8'});
      row.push({text: item.amount.toFixed(2), style: 'size8'});
      row.push({text: item.priceNet.toFixed(2), style: 'size8'});
      row.push({text: item.priceAmountNet.toFixed(2), style: 'size8'});
      row.push({text: item.vat+'%', style: 'size8'});
      row.push({text: item.priceVat.toFixed(2), style: 'size8'});
      row.push({text: item.priceAmountGross.toFixed(2), style: 'size8'});
      data.push(row);
      i++;
    }
    return data;
  }

  getInfoAboutContract() {
    return {
      margin: [75,10,0,0],
      table: {
        widths: ['89%'],
        body: [
          [{text: 'Powyższe rozliczenie dotyczy 42 godz. usług świadczonych w okresie od ' + CalculateDateComponent.getFirstDayOfMonthDate(this.invoice.delivery) + ' do ' + CalculateDateComponent.getLastDayOfMonthDate(this.invoice.delivery) + '.', style: 'size8'}]
        ]
      }
    }
  }

  getVATRate() {
    return {
      alignment: 'center',
      margin: [0,10,0,0],
      text: 'Ogółem według stawek VAT',
      fontSize: 9
    }
  }

  setPaymentData(payment: any) {
    payment.push(this.getTaxInfo());
    payment.push(this.getPaymentType());
    payment.push(this.getBankName());
    payment.push(this.getBankAccount());
  }

  getTaxInfo() {
    return {
      margin: [75,10,0,0],
      columns: [
        {
          alignment: 'center',
          table: {
            widths: ['100%'],
            body: [
              [{text: 'RAZEM: ', alignment: 'right'}],
              [{text: 'Do zapłaty: ' + this.invoice.gross.toFixed(2), alignment: 'left', fontSize: 8}],
              [{text: 'Słownie: ' + ConvertNumberToWord.convert(this.invoice.gross), alignment: 'left', fontSize: 8}]
            ]
          },layout: 'noBorders'
        },
        {
          margin: [0,0,6,0],
          table: {
            widths: ['22%', '10%', '22%', '22%'],
            body: this.taxInfo()
          }
        }
      ]
    }
  }

  taxInfo() {
    let totalTax = [];
    totalTax.push([
      {text: this.invoice.net.toFixed(2), style: 'size8'},
      {text: ''},
      {text: this.invoice.vat.toFixed(2), style: 'size8'},
      {text: this.invoice.gross.toFixed(2), style: 'size8'}]);
    this.getTaxSortedByVAT(totalTax);
    return totalTax;
  }

  getTaxSortedByVAT(totalTax: any) {
    let vat = [[0.00, 0.00, 0.00], [0.00, 0.00, 0.00], [0.00, 0.00, 0.00], [0.00, 0.00, 0.00], [0.00, 0.00, 0.00]];
    for(let item of this.invoice.invoiceItems) {
      let row = [item.priceAmountNet, item.priceVat, item.priceAmountGross];
      if(item.vat == 23) {
        vat[0] = [vat[0][0]+row[0], vat[0][1]+row[1], vat[0][2]+row[2]];
      } else if(item.vat == 8) {
        vat[1] = [vat[1][0]+row[0], vat[1][1]+row[1], vat[1][2]+row[2]];
      } else if(item.vat == 5) {
        vat[2] = [vat[2][0]+row[0], vat[2][1]+row[1], vat[2][2]+row[2]];
      } else if(item.vat == 4) {
        vat[3] = [vat[3][0]+row[0], vat[3][1]+row[1], vat[3][2]+row[2]];
      } else if(item.vat == 0) {
        vat[4] = [vat[4][0]+row[0], vat[4][1]+row[1], vat[4][2]+row[2]];
      }
    }

    totalTax.push([{text: vat[0][0].toFixed(2), style: 'size8'}, {text: '23%', style: 'size8'}, {text: vat[0][1].toFixed(2), style: 'size8'}, {text: vat[0][2].toFixed(2), style: 'size8'}]);
    totalTax.push([{text: vat[1][0].toFixed(2), style: 'size8'}, {text: '8%', style: 'size8'}, {text: vat[1][1].toFixed(2), style: 'size8'}, {text: vat[1][2].toFixed(2), style: 'size8'}]);
    totalTax.push([{text: vat[2][0].toFixed(2), style: 'size8'}, {text: '5%', style: 'size8'}, {text: vat[2][1].toFixed(2), style: 'size8'}, {text: vat[2][2].toFixed(2), style: 'size8'}]);
    totalTax.push([{text: vat[3][0].toFixed(2), style: 'size8'}, {text: '4%', style: 'size8'}, {text: vat[3][1].toFixed(2), style: 'size8'}, {text: vat[3][2].toFixed(2), style: 'size8'}]);
    totalTax.push([{text: vat[4][0].toFixed(2), style: 'size8'}, {text: '0%', style: 'size8'}, {text: vat[4][1].toFixed(2), style: 'size8'}, {text: vat[4][2].toFixed(2), style: 'size8'}]);
  }

  getPaymentType() {
    return {
      margin: [75,10,0,0],
      text: 'Forma płatności: Przelewem na poniżesze konto w terminie 10 dni', style: 'size8'
    }
  }

  getBankName() {
    return {
      margin: [75,10,0,0],
      text: 'Bank: Millennium', style: 'size8'
    }
  }

  getBankAccount() {
    return {
      margin: [75,10,0,0],
      text: 'Konto: 86 1160 2202 0000 0003 6615 2625', style: 'size8'
    }
  }

  setSignature(signature: any) {
    signature.push(this.getSignatures())
  }

  getSignatures() {
    return {
      margin: [0,20,0,0],
      columns: [
        {
          margin: [75,0,0,0],
          table: {
            widths: ['65%'],
            body: [
              [{text: '', margin: [0,40,0,0]}],
              [{text: 'Imie i nazwisko osoby uprawnionej\ndo wystawienia faktury VAT', style: 'size6'}]
            ]
          }
        },
        {
          margin: [90,0,0,0],
          table: {
            widths: ['70%'],
            body: [
              [{text: '', margin: [0,40,0,0]}],
              [{text: 'Imie i nazwisko osoby uprawnionej\ndo odbioru faktury VAT', style: 'size6'}]
            ]
          }
        }
      ]
    }
  }

}
