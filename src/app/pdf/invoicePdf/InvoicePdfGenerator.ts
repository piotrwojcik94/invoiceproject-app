import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoicePdfCreator} from "./InvoicePdfCreator";
import {Document} from "../../entities/invoice/Document.model";
import pdfMake from 'pdfmake/build/pdfmake.js';
import pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export class InvoicePdfGenerator {

  generate(invoice: Invoice) {
    let pdfCreator = new InvoicePdfCreator(invoice);
    pdfCreator.setHeader().setContractors().setInvoiceItems().setPaymentData().setSignature();
    let pdfData = pdfCreator.create();

    const documentDefinition = {
      pageMargins: [ 10,30,10,10 ],
      content: pdfData,

      styles: {
        stamp: {
          margin: [0,10,0,0],
          color: '#C0C0C0',
          fontSize: 8
        },
        small: {
          fontSize: 8,
          margin: [0,5,0,0]
        },
        small2: {
          fontSize: 8,
          margin: [0,8,0,0],
          alignment: 'right'
        },
        contractor: {
          fontSize: 8,
          fillColor: '#989898'
        },
        size8 : {
          fontSize: 8
        },
        size6: {
          alignment: 'center',
          fontSize: 6
        }
      },
      defaultStyle: {
        columnGap: 20
      }
    };
    //@ts-ignore
    pdfMake.createPdf(documentDefinition).getBlob((blob: Blob) => {
      invoice.document = new Document(0, blob, 'name');
    });
  }
}
