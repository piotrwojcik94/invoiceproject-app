import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {Title} from "@angular/platform-browser";
import {LoginUserComponent} from "./login-user.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HeaderService} from "../header/header.service";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('login-user', () => {
  let titleService: Title;
  let component: LoginUserComponent;
  let fixture: ComponentFixture<LoginUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatInputModule, MatIconModule, ReactiveFormsModule, BrowserAnimationsModule],
      declarations: [LoginUserComponent],
      providers: [HeaderService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have title as 'Zaloguj się'`, () => {
    titleService = TestBed.get(Title);
    expect(titleService.getTitle()).toBe('Zaloguj się');
  });
});
