import {Component} from "@angular/core";
import {LoginService} from "../services/login.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {EmailValidator} from "../validators/EmailValidator";
import {Title} from "@angular/platform-browser";
import {NotEmptyValidator} from "../validators/NotEmptyValidator";
import {AppSettings} from "../app-settings";
import {UserService} from "../services/user.service";
import {HeaderService} from "../header/header.service";
import {HttpResponse} from "../services/http-response";

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent {

  loginForm: FormGroup;
  userLogin: FormControl = new FormControl('', [EmailValidator.validateEmail as ValidatorFn], null);
  userPassword: FormControl = new FormControl('', [NotEmptyValidator.validateNotEmpty as ValidatorFn]);
  hide = true;
  isServerError = false;
  credentials = {username: '', password: ''};

  constructor(private loginService: LoginService,
              private router: Router,
              private headerService: HeaderService,
              private userService: UserService,
              private titleService: Title) {
    this.titleService.setTitle('Zaloguj się');
    this.loginForm = new FormGroup({
      userLogin: this.userLogin,
      userPassword: this.userPassword
    });
  }

  login() {
    this.credentials.username = this.userLogin.value;
    this.credentials.password = this.userPassword.value;
    this.loginService.authenticate(this.credentials, (data: HttpResponse) => {
      if (data == HttpResponse.connection_error) {
        this.userLogin.setErrors({loginError: 'Problem z serwerem, spróbuj później!'});
        this.isServerError = true;
      } else if (data == HttpResponse.wrong_credentials) {
        this.userLogin.setErrors({loginError: 'Zły login lub hasło!'});
      }
    });
  }

  onPasswordChange() {
    this.userLogin.updateValueAndValidity();
    this.isServerError = false;
  }

  onLoginChange() {
    this.isServerError = false;
  }
}
