export class AppSettings {
  public static readonly BASE_URI = 'http://localhost:8080/InvoiceAPI';
  public static readonly AUTHENTICATION_TOKEN_PREFIX = 'PW';
  public static readonly AUTHENTICATION_TOKEN = 'authenticationToken';
  public static readonly AUTHENTICATION_FUTURE_TOKEN = 'authenticationFutureToken';
  public static readonly USER_NIP = 'userNIP';
  public static readonly CONTRACTOR = 'contractor'
}
