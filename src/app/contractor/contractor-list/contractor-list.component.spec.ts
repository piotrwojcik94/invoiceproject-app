import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorListComponent } from './contractor-list.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MockComponent} from "ng-mocks";
import {PageNavigationComponent} from "../../navigation/page-navigation/page-navigation.component";
import {provideAnimations} from "@angular/platform-browser/animations";

describe('ContractorListComponent', () => {
  let component: ContractorListComponent;
  let fixture: ComponentFixture<ContractorListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, MatInputModule, FormsModule],
      declarations: [ContractorListComponent,MockComponent(PageNavigationComponent)],
      providers: [provideAnimations()]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContractorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
