import {Component, OnInit} from '@angular/core';
import {Contractor} from "../../entities/invoice/Contractor.model";
import {ContractorService} from "../../services/contractor.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-contractor-list',
  templateUrl: './contractor-list.component.html',
  styleUrl: './contractor-list.component.css'
})
export class ContractorListComponent implements OnInit {
  columns: String[] = ["Lp.", "Nazwa", "NIP", "Ulica", "Nr domu", "Nr mieszkania", "Kod pocztowy", "Miasto", "Więcej"];
  contractors: Contractor[] = [];
  page: number = 0;
  size: number = 10;
  totalPages: number = 1;
  filter = '';

  constructor(private contractorService: ContractorService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.updateList(0);
  }

  updateList(page: number) {
    this.contractorService.getContractors(page, this.size, this.filter).subscribe(data => {
      this.contractors = data.content;
      this.totalPages = data.totalPages;
    });
  }

  editContractor(i: number) {
    this.router.navigate(['edit'], {relativeTo: this.route, state: {contractor: this.contractors[i]}});
    console.log('przejdz do edycji ' + i);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filter = filterValue.trim().toLowerCase();
    this.updateList(0);
  }
}
