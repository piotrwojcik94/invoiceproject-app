import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorEditComponent } from './contractor-edit.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {Contractor} from "../../entities/invoice/Contractor.model";
import {provideAnimations} from "@angular/platform-browser/animations";

describe('ContractorEditComponent', () => {
  let component: ContractorEditComponent;
  let fixture: ComponentFixture<ContractorEditComponent>;

  beforeEach(async () => {
    spyOnProperty(window.history, 'state').and.returnValue({ contractor: new Contractor(null) });
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, FormsModule, MatInputModule],
      declarations: [ContractorEditComponent],
      providers: [provideAnimations()]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContractorEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
