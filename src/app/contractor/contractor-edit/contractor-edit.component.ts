import {Component, OnInit} from '@angular/core';
import {Contractor} from "../../entities/invoice/Contractor.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ContractorService} from "../../services/contractor.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-contractor-edit',
  templateUrl: './contractor-edit.component.html',
  styleUrl: './contractor-edit.component.css'
})
export class ContractorEditComponent implements OnInit {
  contractor: Contractor;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private contractorService: ContractorService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.contractor = history.state.contractor;
  }

  onSubmit(): void {
    this.contractorService.updateContractor(this.contractor).subscribe({
      complete: () => {
        this.snackBar.open('Kontraktor pomyślnie zmodyfikowany !', '', {duration: 2000}).afterDismissed().subscribe(() => {
          this.onCancel();
        });
      }, error: err => {
        console.error('Error updating contractor', err);
      }
    });
  }

  onCancel(): void {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
