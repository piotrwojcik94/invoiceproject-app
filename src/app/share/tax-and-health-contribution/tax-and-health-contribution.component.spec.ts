import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAndHealthContributionComponent } from './tax-and-health-contribution.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('TaxAndHealthContributionComponent', () => {
  let component: TaxAndHealthContributionComponent;
  let fixture: ComponentFixture<TaxAndHealthContributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ TaxAndHealthContributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAndHealthContributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
