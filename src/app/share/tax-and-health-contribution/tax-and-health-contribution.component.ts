import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {KPiR} from "../../entities/invoice/KPiR.model";
import {MonthSummary} from "../../entities/invoice/month-summary.model";
import {KpirService} from "../../services/kpir.service";
import {SelectedDate} from "../../entities/invoice/SelectedDate.model";
import {SelectedDateFromDate} from "../../entities/invoice/SelectedDateFromDate.model";
import {TaxCalculator} from "../../calculator/tax-calculator/tax-calculator";
import {TaxTypes} from "../../entities/tax-types";
import {
  HealthContributionCalculator
} from "../../calculator/health-contributions-calculator/health-contribution-calculator";

@Component({
  selector: 'app-tax-and-health-contribution',
  templateUrl: './tax-and-health-contribution.component.html',
  styleUrls: ['./tax-and-health-contribution.component.css']
})
export class TaxAndHealthContributionComponent implements OnChanges {
  @Input()
  kpir: KPiR;
  previousKpir: KPiR = new KPiR(null, new Date());
  monthSummary: MonthSummary = new MonthSummary();
  date: SelectedDate = new SelectedDateFromDate(new Date());

  columns : String[] = ["Przychód", "Koszt", "Dochód", "Podatek dochodowy", "Składka zdrowotna", "Łączne obciążenie", "Zysk"];

  constructor(private kpirService: KpirService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.kpir) {
      return;
    }
    this.loadPreviousKpir();
  }

  private loadPreviousKpir(): void {
    let date: SelectedDate = new SelectedDateFromDate(this.kpir.date);
    if (date.month == 0) {
      date.month = 12;
      date.year--;
    }
    this.kpirService.getKpir(date)?.subscribe(
      previousKpir => {
        this.previousKpir = previousKpir;
        this.calculateMonthSummary();
      },
      error => {
        console.error('Error loading previous KPIR:', error);
      }
    );
  }

  public calculateAndSaveMonthSummary(): void {
    this.calculateMonthSummary();
    this.kpir.taxToPay = this.monthSummary.tax;
    console.log(this.kpir)
    this.kpirService.save(this.kpir)?.subscribe(
      obj => {
        console.log('KPIR saved successfully:', obj);
      },
      error => {
        console.error('Error saving KPIR:', error);
      }
    );
  }

  private calculateMonthSummary(): void {
    this.monthSummary.income = this.kpir.incomeAndExpensesCurrentPage.totalIncome;
    this.monthSummary.expense = this.kpir.incomeAndExpensesCurrentPage.totalExpenses;
    this.monthSummary.profit = this.monthSummary.income - this.monthSummary.expense;
    this.monthSummary.tax = new TaxCalculator().calculate(this.monthSummary.profit, this.kpir.date.getFullYear(), this.kpir.date.getMonth(), this.kpir.incomeAndExpensesYear.totalIncome, this.getPreviousMonthTaxToPay(), TaxTypes.progressive);
    this.monthSummary.healthContribution = new HealthContributionCalculator().calculate(this.getPreviousMonthProfit(), this.date.year, this.date.month, TaxTypes.progressive);
    this.monthSummary.totalTax = this.monthSummary.tax + this.monthSummary.healthContribution;
    this.monthSummary.totalProfit = this.monthSummary.profit - this.monthSummary.totalTax;
  }

  private getPreviousMonthProfit() {
    let previousMonthProfit = 0;
    if (this.previousKpir != null) {
      previousMonthProfit = this.previousKpir.incomeAndExpensesCurrentPage.totalIncome - this.previousKpir.incomeAndExpensesCurrentPage.totalExpenses;
    }
    return previousMonthProfit;
  }

  private getPreviousMonthTaxToPay() {
    let taxToPay = 0;
    if (this.previousKpir != null) {
      taxToPay = this.previousKpir.taxToPay;
    }
    return taxToPay;
  }
}
