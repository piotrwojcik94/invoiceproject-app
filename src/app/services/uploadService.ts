import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'})export class UploadFileService {

  constructor(private httpClient: HttpClient) {
  }

  getPdf(id: number) {
    const httpOptions = {
      responseType: 'blob' as 'json'
    };
    return this.httpClient.get('http://localhost:8080/InvoiceAPI/document/download/' + id, httpOptions);
  }
}
