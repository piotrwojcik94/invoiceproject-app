import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AppSettings} from "../app-settings";

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (AuthInterceptorService.isValidRequest(req.url)) {
      let token: string | null = sessionStorage.getItem(AppSettings.AUTHENTICATION_TOKEN);

      if (token != null) {
        const authReq = req.clone({
          headers: req.headers.set('Authorization', token)
        });
        return next.handle(authReq);
      }
    }
    return next.handle(req);
  }

  private static isValidRequest(requestUrl: string) {
    return requestUrl.startsWith(AppSettings.BASE_URI);
  }
}
