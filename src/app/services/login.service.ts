import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {TokenRequest} from "./TokenRequest";
import {TokenResponse} from "./TokenResponse";
import {AuthenticationService} from "./AuthenticationService.service";
import {Router} from "@angular/router";
import {ContractorService} from "./contractor.service";
import {UserService} from "./user.service";
import {AppSettings} from "../app-settings";
import {HeaderService} from "../header/header.service";
import {HttpResponse} from "./http-response";

@Injectable({
  providedIn: "root"
})
export class LoginService {

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService,
              private contractorService: ContractorService,
              private userService: UserService,
              private headerService: HeaderService,
              private router: Router) {}

  authenticate(tokenRequest: TokenRequest, callback: (data: HttpResponse) => void) {
    this.authenticationService.getToken(tokenRequest).subscribe(
      (response: TokenResponse) => {
        if (!(null === response.token)) {
          this.setUserLoginData(tokenRequest.username);
          this.authenticationService.setStoredToken(response);
          this.router.navigateByUrl("/");
        } else {
          console.log("Wrong credentials!");
          callback.call('test',HttpResponse.wrong_credentials);
        }
      },
      error => {
        console.log("Error logging in user " + tokenRequest.username + ": " + error.error);
        callback.call('test',HttpResponse.connection_error);
      }
    );
  }

  setUserLoginData(userName: string) {
    this.userService.getUserNIPByUserLogin(userName).subscribe(data => {
      sessionStorage.setItem(AppSettings.USER_NIP, data.nip.toString());
      this.headerService.getContractorData();
    });
  }
}
