import {HttpParams} from "@angular/common/http";
import {SelectedDate} from "../entities/invoice/SelectedDate.model";
import {InvoiceType} from "../entities/invoice/InvoiceType";
import {CalculateDateComponent} from "../tools/calculate-date.component";

export class HttpParamsCreator {
  public static withYear(year: number): HttpParams {
    return new HttpParams().set('year', year);
  }

  public static withNip(nip: string): HttpParams {
    return new HttpParams().set('nip', nip);
  }

  public static withFirstNipNumbers(nip: string): HttpParams {
    return new HttpParams().set('firstNipNumber', nip);
  }

  public static withLogin(login: string): HttpParams {
    return new HttpParams().set('login', login);
  }

  public static withDate(date: SelectedDate): HttpParams {
    return new HttpParams().set('year', date.year).set('month', date.month);
  }

  public static withDateAndInvoiceType(date: SelectedDate, invoiceType: InvoiceType[]) {
    return this.withDate(date).set('invoiceType', invoiceType.toString())
  }

  public static withFormattedDate() {
    return new HttpParams().set('date', CalculateDateComponent.getFormattedDate(new Date()));
  }

  public static withPageAndSizeAndFilter(page: number, size: number, filter: string) {
    return new HttpParams().set('page', page).set('size', size).set('filter', filter);
  }
}
