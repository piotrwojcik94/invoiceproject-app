import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {User} from "../entities/invoice/User.model";
import {AppSettings} from "../app-settings";
import {HttpParamsCreator} from "./HttpParamsCreator";

@Injectable({
  providedIn: "root"
})
export class UserService {
  subject = new BehaviorSubject<any>(null);

  constructor(private httpClient: HttpClient) {}

  public saveUser(user: User) : Observable<Object> {
    return this.httpClient.post(AppSettings.BASE_URI + '/user/newUser', {
      user: user
    });
  }

  public updateUser(user: User) {
    return this.httpClient.post(AppSettings.BASE_URI + '/user/updateUser', {
      user: user,
      taxationForms: user.taxationForms
    });
  }

  public isEmailUnique(email: string) {
    return this.httpClient.get<boolean>(AppSettings.BASE_URI + '/user/isEmailUnique', {
      params: HttpParamsCreator.withLogin(email)
    });
  }

  public getUserByNIP(nip: string) {
    return this.httpClient.get<User>(AppSettings.BASE_URI + '/user/getUserByUserNIP', {
      params: HttpParamsCreator.withNip(nip)
    });
  }

  public isNIPUnique(nip: string) {
    return this.httpClient.get<boolean>(AppSettings.BASE_URI + '/user/isNIPUnique', {
      params: HttpParamsCreator.withNip(nip)
    });
  }

  public getUserNIPByUserLogin(login: string) {
    return this.httpClient.get<User>(AppSettings.BASE_URI + '/user/getUserByUserLogin', {
      params: HttpParamsCreator.withLogin(login)
    });
  }
}
