import {HttpClient, HttpHeaders} from "@angular/common/http";
import {TokenRequest} from "./TokenRequest";
import {TokenResponse} from "./TokenResponse";
import {Injectable} from "@angular/core";
import {Observable, interval} from 'rxjs';
//@ts-ignore
import * as jwt_decode from "jwt-decode";
import {AppSettings} from "../app-settings";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService
{
  private authScheduler: Observable<any> = interval(10000);

  constructor(private httpClient : HttpClient, private router: Router) {
    this.scheduleReAuthentication();
  }

  public getToken(credentials : TokenRequest) : Observable<TokenResponse> {
    return this.httpClient.post<TokenResponse>(AppSettings.BASE_URI + "/user/login", credentials);
  }

  public renewToken() {
    let token: string|null = sessionStorage.getItem(AppSettings.AUTHENTICATION_FUTURE_TOKEN);
    if (token != null) {
      const httpOptions = {
        headers: new HttpHeaders(
          {
            Authorization: token
          }
        )
      };
      return this.httpClient.get<TokenResponse>(AppSettings.BASE_URI + "/user/login", httpOptions)
    }
    return null;
  }

  public getLoggedInUserName() : string {
    const decoded: any = jwt_decode(this.getStoredToken());
    return decoded.sub;
  }

  setStoredToken(authResponse : TokenResponse) {
    sessionStorage.setItem(AppSettings.AUTHENTICATION_TOKEN, AppSettings.AUTHENTICATION_TOKEN_PREFIX + authResponse.token);
    sessionStorage.setItem(AppSettings.AUTHENTICATION_FUTURE_TOKEN, AppSettings.AUTHENTICATION_TOKEN_PREFIX + authResponse.futureToken);
  }

  getStoredToken() {
    return sessionStorage.getItem(AppSettings.AUTHENTICATION_TOKEN);
  }

  isLoggedIn() : boolean {
    let loggedIn: boolean = false;
    if (!this.getStoredToken())
      loggedIn = false;
    else {
      let tokenExpirationDate: Date|null = this.getStoredTokenExpiration();
      if (tokenExpirationDate != null && tokenExpirationDate.valueOf() > new Date().valueOf()) {
        loggedIn = true;
      }
    }
    return loggedIn;
  }

  private getStoredTokenExpiration() : Date|null {
    const decoded: any = jwt_decode(this.getStoredToken());

    if(decoded.exp === undefined)
      return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);

    return date;
  }

  private scheduleReAuthentication() {
    this.authScheduler.subscribe(() => {
      if (this.isLoggedIn()) {
        console.log("checking token ...");

        let timeLeft: number;
        let tokenExpirationDate: Date|null = this.getStoredTokenExpiration();
        if (tokenExpirationDate != null) {
          timeLeft = tokenExpirationDate.valueOf() - new Date().valueOf();
          if ((timeLeft / 1000) < 30) {
            this.renewToken()?.subscribe(
              (newToken: TokenResponse) => {
                this.setStoredToken(newToken);
                console.log('Token renewal was successful!');
              },
              error => {
                console.log('Unable to renew token: ' + error.error.error);
                this.logOut(true);
              }
            );
          }
        }
      }
    });
  }

  logOut(error : boolean) {
    console.log('Logout');
    this.router.navigateByUrl("/");
    sessionStorage.removeItem(AppSettings.CONTRACTOR);
    sessionStorage.removeItem(AppSettings.USER_NIP);
    sessionStorage.removeItem(AppSettings.AUTHENTICATION_TOKEN);
    sessionStorage.removeItem(AppSettings.AUTHENTICATION_FUTURE_TOKEN);
  }
}
