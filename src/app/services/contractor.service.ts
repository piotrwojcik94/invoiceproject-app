import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Contractor} from "../entities/invoice/Contractor.model";
import {ContractorCeidg} from "../entities/invoice/ContractorCeidg.model";
import {AppSettings} from "../app-settings";
import {HttpParamsCreator} from "./HttpParamsCreator";
import {Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ContractorService {

  constructor(private httpClient: HttpClient) {}

  public getContractorFromCeidg(NIP: string) {
    return this.httpClient.get<any>('https://wl-api.mf.gov.pl/api/search/nip/'+NIP+'?&', {
      params: HttpParamsCreator.withFormattedDate()
    }).toPromise();
  }

  public getContractors(page: number, size: number, filter: string) {
    return this.httpClient.get<any>(AppSettings.BASE_URI + '/contractor/contractors', {
      params: HttpParamsCreator.withPageAndSizeAndFilter(page, size, filter)
    });
  }

  public getSuggestions(firstNipNumbers: string): Observable<Contractor[]> {
    return this.httpClient.get<Contractor[]>(AppSettings.BASE_URI + '/contractor/suggestions', {
      params: HttpParamsCreator.withFirstNipNumbers(firstNipNumbers)
    });
  }

  public updateContractor(contractor: Contractor) {
    return this.httpClient.put(AppSettings.BASE_URI + `/contractor/update/${contractor.recid}`, contractor);
  }
}
