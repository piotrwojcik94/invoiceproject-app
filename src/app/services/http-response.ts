export enum HttpResponse {
  wrong_credentials = "wrong_credentials",
  connection_error = "connection_error"
}
