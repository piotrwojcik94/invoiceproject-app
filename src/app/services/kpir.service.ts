import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {KPiR} from "../entities/invoice/KPiR.model";
import {AppSettings} from "../app-settings";
import {KpirAndInvoicesModel} from "../entities/invoice/KpirAndInvoices.model";
import {SelectedDate} from "../entities/invoice/SelectedDate.model";
import {HttpParamsCreator} from "./HttpParamsCreator";

@Injectable({
  providedIn: "root"
})
export class KpirService {

  subject = new BehaviorSubject<any>(null);
  static instance: KpirService;

  constructor(private httpClient: HttpClient) {
    if (!KpirService.instance) {
      KpirService.instance = this;
    }
    return KpirService.instance;
  }

  public getKpir(date: SelectedDate) {
    return this.httpClient.get<KPiR>(AppSettings.BASE_URI + '/Invoice/getKpir', {
      params: HttpParamsCreator.withDate(date)
    });
  }

  public getKpirAndInvoices(date: SelectedDate) {
    return this.httpClient.get<KpirAndInvoicesModel>(AppSettings.BASE_URI + '/Invoice/getKpirAndInvoices', {
      params: HttpParamsCreator.withDate(date)
    });
  }

  public save(kpir: KPiR) {
    return this.httpClient.post(AppSettings.BASE_URI + '/Invoice/kpir/', {
      kpir: kpir
    });
  }
}
