import {HttpClient, HttpRequest} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {AppSettings} from "../app-settings";
import {Invoice} from "../entities/invoice/Invoice.model";
import {InvoiceType} from "../entities/invoice/InvoiceType";
import {SelectedDate} from "../entities/invoice/SelectedDate.model";
import {HttpParamsCreator} from "./HttpParamsCreator";

@Injectable({
  providedIn: "root"
})
export class InvoiceService {

  subject = new BehaviorSubject<any>(null);

  constructor(private httpClient: HttpClient) {}

  public getInvoicesByDateAndType(date: SelectedDate, invoiceType: InvoiceType[]) {
    return this.httpClient.get<Invoice[]>(AppSettings.BASE_URI + '/Invoice/', {
      params: HttpParamsCreator.withDateAndInvoiceType(date, invoiceType)
    });
  }

  public deleteInvoice(invoice: Invoice) {
      return this.httpClient.delete(AppSettings.BASE_URI + '/Invoice/deleteInvoice/' + invoice.recid);
  }

  public saveInvoice(invoice: Invoice) : Observable<any> {
    const data: FormData = new FormData();
    if (invoice.document?.file != null) {
      data.append('file', invoice.document.file);
      data.append('documentId', invoice.document.recid.toString());
      invoice.document = null;
    }
    data.append('invoice', JSON.stringify(invoice));
    const newRequest = new HttpRequest('POST', AppSettings.BASE_URI + '/Invoice/newInvoice/', data, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.httpClient.request(newRequest);
  }

  public updateInvoice(invoice: Invoice) {
    return this.httpClient.post(AppSettings.BASE_URI + '/Invoice/updateInvoice/', {
      invoice: invoice,
      invoiceItems: invoice.invoiceItems
    });
  }
}
