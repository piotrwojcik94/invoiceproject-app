import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {AppSettings} from "../app-settings";
import {HttpParamsCreator} from "./HttpParamsCreator";

@Injectable({
  providedIn: "root"
})
export class StatisticService{

  subject = new BehaviorSubject<any>(null);

  constructor(private httpClient: HttpClient) {}

  public getIncomeAndExpense(year: number) {
    return this.httpClient.get<any>(AppSettings.BASE_URI + '/Statistic/incomeAndExpense', {
      params: HttpParamsCreator.withYear(year)
    });
  }

  public getIncomeAndExpenseAscending(year: number) {
    return this.httpClient.get<any>(AppSettings.BASE_URI + '/Statistic/incomeAndExpenseAscending', {
      params: HttpParamsCreator.withYear(year)
    });
  }

  public getVatMonthly(year: number) {
    return this.httpClient.get<any>(AppSettings.BASE_URI + '/Statistic/vat', {
      params: HttpParamsCreator.withYear(year)
    });
  }

  public getVatMonthlyAscending(year: number) {
    return this.httpClient.get<any>(AppSettings.BASE_URI + '/Statistic/vatAscending', {
      params: HttpParamsCreator.withYear(year)
    });
  }
}
