import {TaxTypes} from "../../entities/tax-types";
import {HealthContributionRates} from "./health-contribution-rates";
import {ProgressiveHealthContribution} from "./progressive-health-contribution";

interface HealthContributionRatePeriod {
  startDate: Date;
  endDate: Date;
  rate: number;
}

export class HealthContributionCalculator {
  private static HEALTH_CONTRIBUTION_RATES_BEFORE_2022: HealthContributionRatePeriod[] = [
    { startDate: new Date(2018, 1, 1), endDate: new Date(2019, 0, 31), rate: 319.94 },
    { startDate: new Date(2019, 1, 1), endDate: new Date(2020, 0, 31), rate: 342.32 },
    { startDate: new Date(2020, 1, 1), endDate: new Date(2021, 0, 31), rate: 362.34 },
    { startDate: new Date(2021, 1, 1), endDate: new Date(2022, 0, 31), rate: 381.81 }
  ];

  public calculate(income: number, year: number, month: number, taxType: TaxTypes) {
    const date = new Date(year, month, 1);
    const contribution = this.getHealthContributionForDate(date);

    if (contribution !== null) {
      return contribution;
    }

    if (taxType === TaxTypes.progressive) {
      return this.calculateProgressiveHealthContribution(income, year, month);
    }

    return 0;
  }

  private getHealthContributionForDate(date: Date): number | null {
    const foundPeriod = HealthContributionCalculator.HEALTH_CONTRIBUTION_RATES_BEFORE_2022.find(period =>
      date >= period.startDate && date <= period.endDate
    );
    return foundPeriod ? foundPeriod.rate : null;
  }

  private calculateProgressiveHealthContribution(income: number, year: number, month: number): number {
    const progressiveRate: ProgressiveHealthContribution | null = HealthContributionRates.getProgressiveRate(year, month);
    if (progressiveRate) {
      const tax = income * progressiveRate.taxRate;
      return tax > progressiveRate.minimalTax ? tax : progressiveRate.minimalTax;
    }
    return 0;
  }
}
