import {TaxTypes} from "../../entities/tax-types";
import {HealthContributionCalculator} from "./health-contribution-calculator";

describe('TaxCalculator', () => {
  let healthContributionCalculator: HealthContributionCalculator;

  beforeEach(async () => {
    healthContributionCalculator = new HealthContributionCalculator();
  });

  const testCases = [
    { income: 5000, year: 2025, month: 1, expectedTax: 450, taxType: TaxTypes.progressive },
    { income: 2000, year: 2025, month: 1, expectedTax: 314.96, taxType: TaxTypes.progressive },
    { income: 5000, year: 2024, month: 11, expectedTax: 450, taxType: TaxTypes.progressive },
    { income: 2000, year: 2024, month: 11, expectedTax: 381.78, taxType: TaxTypes.progressive },
    { income: 2000, year: 2025, month: 0, expectedTax: 381.78, taxType: TaxTypes.progressive },
    { income: 5000, year: 2023, month: 11, expectedTax: 450, taxType: TaxTypes.progressive },
    { income: 2000, year: 2023, month: 11, expectedTax: 314.10, taxType: TaxTypes.progressive },
    { income: 2000, year: 2023, month: 0, expectedTax: 270.90, taxType: TaxTypes.progressive },
    { income: 5000, year: 2022, month: 11, expectedTax: 450, taxType: TaxTypes.progressive },
    { income: 2000, year: 2022, month: 11, expectedTax: 270.90, taxType: TaxTypes.progressive },
    { income: 2000, year: 2022, month: 0, expectedTax: 381.81, taxType: TaxTypes.progressive },
    { income: 2000, year: 2021, month: 11, expectedTax: 381.81, taxType: TaxTypes.progressive },
    { income: 2000, year: 2021, month: 0, expectedTax: 362.34, taxType: TaxTypes.progressive },
    { income: 2000, year: 2020, month: 11, expectedTax: 362.34, taxType: TaxTypes.progressive },
    { income: 2000, year: 2020, month: 0, expectedTax: 342.32, taxType: TaxTypes.progressive },
    { income: 2000, year: 2019, month: 11, expectedTax: 342.32, taxType: TaxTypes.progressive },
    { income: 2000, year: 2019, month: 0, expectedTax: 319.94, taxType: TaxTypes.progressive },
    { income: 2000, year: 2018, month: 11, expectedTax: 319.94, taxType: TaxTypes.progressive },
    { income: 2000, year: 2018, month: 7, expectedTax: 319.94, taxType: TaxTypes.progressive },
  ];

  testCases.forEach(({ income, year, month, expectedTax, taxType }) => {
    it(`should calculate the correct tax for ${month + 1}/${year}`, () => {
      const tax = healthContributionCalculator.calculate(income, year, month, taxType);
      expect(tax).toBeCloseTo(expectedTax, 2);
    });
  });
});
