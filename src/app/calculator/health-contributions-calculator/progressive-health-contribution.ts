export class ProgressiveHealthContribution {
  public taxRate: number;
  public minimalTax: number;

  constructor(taxRate: number, minimalTax: number) {
    this.taxRate = taxRate;
    this.minimalTax = minimalTax;
  }
}
