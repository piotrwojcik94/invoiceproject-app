import {ProgressiveHealthContribution} from "./progressive-health-contribution";

export class HealthContributionRates {
  private static ratePeriods = [
    { startDate: new Date(2025, 1, 1), endDate: new Date(2026, 0, 31), rate: new ProgressiveHealthContribution(0.09, 314.96) },
    { startDate: new Date(2024, 1, 1), endDate: new Date(2025, 0, 31), rate: new ProgressiveHealthContribution(0.09, 381.78) },
    { startDate: new Date(2023, 1, 1), endDate: new Date(2024, 0, 31), rate: new ProgressiveHealthContribution(0.09, 314.10) },
    { startDate: new Date(2022, 1, 1), endDate: new Date(2023, 0, 31), rate: new ProgressiveHealthContribution(0.09, 270.90) }
  ];

  public static getProgressiveRate(year: number, month: number): ProgressiveHealthContribution | null {
    const date = new Date(year, month, 1);

    for (const period of HealthContributionRates.ratePeriods) {
      if (date >= period.startDate && date <= period.endDate) {
        return period.rate;
      }
    }

    return null;
  }
}
