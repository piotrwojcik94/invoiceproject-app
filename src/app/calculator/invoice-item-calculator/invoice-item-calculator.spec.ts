import {InvoiceItem} from "../../entities/invoice/InvoiceItem.model";
import {InvoiceItemCalculator} from "./invoice-item-calculator";
import {Invoice} from "../../entities/invoice/Invoice.model";
import {PriceType} from "../../entities/invoice/PriceType";

describe('InvoiceItemCalculator', () => {
  it('should correctly calculate prices in "net" mode', () => {
    let invoice: Invoice = new Invoice(null);
    const item: InvoiceItem = new InvoiceItem({
      priceNet: 100,
      vat: 23,
      amount: 2
    });

    InvoiceItemCalculator.calculateInvoiceAndItemPrices(invoice, item, false, 0);

    expect(item.priceAmountNet).toBe(200);
    expect(item.priceVat).toBe(46);
    expect(item.priceAmountGross).toBe(246);
    expect(invoice.net).toBe(200);
    expect(invoice.vat).toBe(46);
    expect(invoice.gross).toBe(246);
  });

  it('should correctly calculate prices in "gross" mode', () => {
    let invoice: Invoice = new Invoice(null);
    invoice.priceType = PriceType.gross;
    const item: InvoiceItem = new InvoiceItem({
      priceGross: 123,
      vat: 23,
      amount: 2
    });

    InvoiceItemCalculator.calculateInvoiceAndItemPrices(invoice, item, false, 0);

    expect(item.priceAmountGross).toBe(246);
    expect(item.priceNet).toBe(100);
    expect(item.priceAmountNet).toBe(200);
    expect(item.priceVat).toBe(46);
    expect(invoice.net).toBe(200);
    expect(invoice.vat).toBe(46);
    expect(invoice.gross).toBe(246);
  });

  it('should correctly calculate prices in "net" mode during edition', () => {
    let invoice: Invoice = new Invoice(null);
    let item: InvoiceItem = new InvoiceItem({
      priceNet: 100,
      vat: 23,
      amount: 2
    });

    InvoiceItemCalculator.calculateInvoiceAndItemPrices(invoice, item, false, 0);

    item = new InvoiceItem({
      priceNet: 100,
      vat: 23,
      amount: 1
    });

    InvoiceItemCalculator.calculateInvoiceAndItemPrices(invoice, item, true, 0);

    expect(item.priceAmountNet).toBe(100);
    expect(item.priceVat).toBe(23);
    expect(item.priceAmountGross).toBe(123);
    expect(invoice.net).toBe(100);
    expect(invoice.vat).toBe(23);
    expect(invoice.gross).toBe(123);
  });

  it('should correctly calculate prices in "net" mode during delete item', () => {
    let invoice: Invoice = new Invoice(null);
    let item: InvoiceItem = new InvoiceItem({
      priceNet: 100,
      vat: 23,
      amount: 2
    });

    InvoiceItemCalculator.calculateInvoiceAndItemPrices(invoice, item, false, 0);

    InvoiceItemCalculator.calculateInvoiceSummaryByRemoveInvoiceItem(invoice, item);

    expect(invoice.net).toBe(0);
    expect(invoice.vat).toBe(0);
    expect(invoice.gross).toBe(0);
  });
});
