import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoiceItem} from "../../entities/invoice/InvoiceItem.model";
import {PriceType} from "../../entities/invoice/PriceType";

export class InvoiceItemCalculator {
  private static calculateInvoiceItemPricesInNet(item: InvoiceItem) {
    item.priceAmountNet = +(item.priceNet * item.amount).toFixed(2);
    item.priceVat = +(item.priceAmountNet * item.vat * 0.01).toFixed(2);
    item.priceAmountGross = +(item.priceAmountNet + item.priceVat).toFixed(2);
  }

  private static calculateInvoiceItemPricesInGross(item: InvoiceItem) {
    item.priceAmountGross = +(item.priceGross * item.amount).toFixed(2);
    item.priceNet = +(item.priceGross / (1 + item.vat * 0.01)).toFixed(2);
    item.priceAmountNet = +(item.priceNet * item.amount).toFixed(2);
    item.priceVat = +(item.priceAmountNet * item.vat * 0.01).toFixed(2);
  }

  private static calculateInvoiceSummary(invoice: Invoice, item: InvoiceItem, operation: 'add' | 'remove') {
    const factor = operation === 'add' ? 1 : -1;
    invoice.net += factor * item.priceAmountNet;
    invoice.vat += factor * item.priceVat;
    invoice.gross += factor * item.priceAmountGross;
  }

  private static calculateInvoiceSummaryByAddInvoiceItem(invoice: Invoice, item: InvoiceItem) {
    this.calculateInvoiceSummary(invoice, item, 'add');
  }

  public static calculateInvoiceSummaryByRemoveInvoiceItem(invoice: Invoice, item: InvoiceItem) {
    this.calculateInvoiceSummary(invoice, item, 'remove');
  }

  public static calculateInvoiceAndItemPrices(invoice: Invoice, invoiceItem: InvoiceItem, editMode: boolean, selectedInvoiceItem: number) {
    if(invoice.priceType == PriceType.net) {
      this.calculateInvoiceItemPricesInNet(invoiceItem);
    } else {
      this.calculateInvoiceItemPricesInGross(invoiceItem);
    }
    if(editMode) {
      this.calculateInvoiceSummaryByRemoveInvoiceItem(invoice, invoice.invoiceItems[selectedInvoiceItem]);
      invoice.invoiceItems[selectedInvoiceItem] = invoiceItem;
      this.calculateInvoiceSummaryByAddInvoiceItem(invoice, invoiceItem);
    } else {
      invoice.invoiceItems.push(invoiceItem);
      this.calculateInvoiceSummaryByAddInvoiceItem(invoice, invoiceItem);
    }
  }
}
