import {ProgressiveTax} from "./progressive-tax";

export class TaxRates {
  public static getFlatTaxRate() {
    return 0.19;
  }

  public static getProgressiveTaxRate(year: number) {
    if (year >= 2022) {
      return new ProgressiveTax(30000, 120000, 0.12, 0.32);
    } else {
      return new ProgressiveTax(8000, 85528, 0.17, 0.32);
    }
  }

  public static getLumpSumTaxRate() {
    return 0.12;
  }
}

