import {TaxTypes} from "../../entities/tax-types";
import {TaxRates} from "./tax-rates";
import {ProgressiveTax} from "./progressive-tax";

export class TaxCalculator {
  public calculate(income: number, year: number, month: number, accumulatedIncomeForYear: number, previousMonthReduction: number, taxType: TaxTypes) {
    let taxAmount = 0;
    if (taxType === TaxTypes.progressive) {
      return this.calculateProgressiveTax(income, year, month, accumulatedIncomeForYear, previousMonthReduction)
    }
    return taxAmount;
  }

  private calculateProgressiveTax(income: number, year: number, month: number, accumulatedIncomeForYear: number, previousMonthReduction: number) {
    const taxRates: ProgressiveTax = TaxRates.getProgressiveTaxRate(year);
    const monthlyTaxFreeAmount = taxRates.getMonthlyTaxReduction();
    let taxAmount = 0;
    if (accumulatedIncomeForYear > taxRates.firstTaxBracketLimit) {
      taxAmount = this.calculateSecondBracketTax(income, taxRates);
    } else if (accumulatedIncomeForYear + income <= taxRates.firstTaxBracketLimit) {
      taxAmount = this.calculateFirstBracketTax(income, taxRates);
    } else {
      const firstBracketIncome = taxRates.firstTaxBracketLimit - accumulatedIncomeForYear;
      const firstBracketTax = this.calculateFirstBracketTax(firstBracketIncome, taxRates);

      const secondBracketIncome = accumulatedIncomeForYear + income - taxRates.firstTaxBracketLimit;
      const secondBracketTax = this.calculateSecondBracketTax(secondBracketIncome,taxRates);

      taxAmount = firstBracketTax + secondBracketTax;
    }

    if (month > 0 && previousMonthReduction < 0) {
      taxAmount += previousMonthReduction;
    }

    return taxAmount - monthlyTaxFreeAmount;
  }

  private calculateFirstBracketTax(income: number, taxRates: ProgressiveTax) {
    return income * taxRates.firstTaxRate;
  }

  private calculateSecondBracketTax(income: number, taxRates: ProgressiveTax) {
    return income * taxRates.secondTaxRate;
  }
}
