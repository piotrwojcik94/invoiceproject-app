import {TaxCalculator} from "./tax-calculator";
import {TaxTypes} from "../../entities/tax-types";

describe('TaxCalculator', () => {
  let taxCalculator: TaxCalculator;

  beforeEach(async () => {
    taxCalculator = new TaxCalculator();
  });

  const testCases = [
    {
      income: 3000,
      year: 2024,
      month: 11,
      accumulatedIncomeForYear: 0,
      previousMonthReduction: 0,
      expectedTax: 60,
      description: 'should calculate tax correctly for income below the first tax bracket'
    },
    {
      income: 2000,
      year: 2024,
      month: 11,
      accumulatedIncomeForYear: 3000,
      previousMonthReduction: 0,
      expectedTax: -60,
      description: 'should calculate tax less than 0'
    },
    {
      income: 10000,
      year: 2024,
      month: 11,
      accumulatedIncomeForYear: 5000,
      previousMonthReduction: -60,
      expectedTax: 840,
      description: 'should calculate tax with previous month reduction'
    },
    {
      income: 10000,
      year: 2024,
      month: 0,
      accumulatedIncomeForYear: 5000,
      previousMonthReduction: -60,
      expectedTax: 900,
      description: 'should calculate tax without previous month reduction'
    },
    {
      income: 115000,
      year: 2024,
      month: 11,
      accumulatedIncomeForYear: 15000,
      previousMonthReduction: 0,
      expectedTax: 15500,
      description: 'should calculate tax from 2 brackets'
    },
    {
      income: 10000,
      year: 2024,
      month: 11,
      accumulatedIncomeForYear: 130000,
      previousMonthReduction: 0,
      expectedTax: 2900,
      description: 'should calculate tax only from second brackets'
    }
  ];

  testCases.forEach(({ income, year, month, accumulatedIncomeForYear, previousMonthReduction, expectedTax, description }) => {
    it(description, () => {
      const tax = taxCalculator.calculate(income, year, month, accumulatedIncomeForYear, previousMonthReduction, TaxTypes.progressive);
      expect(tax).toBeCloseTo(expectedTax, 2);
    });
  });
});
