export class ProgressiveTax {
  public taxFreeAmount: number;
  public firstTaxBracketLimit: number;
  public firstTaxRate: number;
  public secondTaxRate: number;

  constructor(taxFreeAmount: number, firstTaxBracketLimit: number, firstTaxRate: number, secondTaxRate: number) {
    this.taxFreeAmount = taxFreeAmount;
    this.firstTaxBracketLimit = firstTaxBracketLimit;
    this.firstTaxRate = firstTaxRate;
    this.secondTaxRate = secondTaxRate;
  }

  public getMonthlyTaxReduction() {
    return (this.taxFreeAmount / 12) * this.firstTaxRate;
  }
}
