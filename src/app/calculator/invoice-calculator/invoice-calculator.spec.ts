import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoiceCalculator} from "./invoice-calculator";
import {InvoicesSummary} from "../../entities/invoice/invoices-summary.model";

describe('InvoiceCalculatorService', () => {
  let invoice1: Invoice;
  let invoice2: Invoice;
  let invoices: Invoice[];
  let invoicesSummary: InvoicesSummary;

  beforeEach(() => {
    invoice1 = new Invoice(null);
    invoice1.net = 500;
    invoice1.vat = 115;
    invoice1.gross = 615;

    invoice2 = new Invoice(null);
    invoice2.net = 200;
    invoice2.vat = 46;
    invoice2.gross = 246;

    invoices = [invoice1, invoice2];

    invoicesSummary = new InvoicesSummary();

    InvoiceCalculator.calculateInvoicesSummary(invoices, invoicesSummary);
  });

  it('should correctly calculate total net, gross and vat for invoices', () => {
    expect(invoicesSummary.net).toBe(700);
    expect(invoicesSummary.gross).toBe(861);
    expect(invoicesSummary.vat).toBe(161);
  });

  it('should correctly calculate total net, gross and vat for invoices when 1 invoice is deleted', () => {
    InvoiceCalculator.calculateInvoicesSummaryByRemoveInvoice(invoice2, invoicesSummary)

    expect(invoicesSummary.net).toBe(500);
    expect(invoicesSummary.gross).toBe(615);
    expect(invoicesSummary.vat).toBe(115);
  });
});
