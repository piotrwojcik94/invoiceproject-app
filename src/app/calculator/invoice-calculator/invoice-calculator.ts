import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoicesSummary} from "../../entities/invoice/invoices-summary.model";
import {InvoiceType} from "../../entities/invoice/InvoiceType";

export class InvoiceCalculator {
  public static calculateInvoicesSummary(invoices: Invoice[], summary: InvoicesSummary) {
    for (let invoice of invoices) {
      this.calulcate(invoice, summary, 1);
    }
  }

  public static calculateInvoicesSummaryByRemoveInvoice(invoice: Invoice, summary: InvoicesSummary) {
    this.calulcate(invoice, summary, -1)
  }

  private static calulcate(invoice: Invoice, summary: InvoicesSummary, factor: number) {
    summary.net += factor * invoice.net;
    if (invoice.invoiceType !== InvoiceType.no_vat) {
      summary.vat += factor * invoice.vat;
      summary.gross += factor * invoice.gross;
    }
  }
}
