import {KPiR} from "../../entities/invoice/KPiR.model";
import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoiceIncomeAndExpenses} from "../../entities/invoice/InvoiceIncomeAndExpenses.model";
import {KpirCalculator} from "./kpir-calculator";
import {InvoiceType} from "../../entities/invoice/InvoiceType";


describe('KpirCalculator', () => {

  let kpir: KPiR;
  let invoice: Invoice;
  let page: InvoiceIncomeAndExpenses;

  beforeEach(() => {
    kpir = new KPiR(null, new Date());
    invoice = new Invoice(null);
    page = new InvoiceIncomeAndExpenses();
  });

  it('should update KPiR page when adding an sale invoice', () => {
    invoice.invoiceType = InvoiceType.sale;
    invoice.net = 10000;
    KpirCalculator.calculateInvoiceCost(invoice);
    KpirCalculator.calculateKpirByAddItem(kpir, invoice);

    expect(kpir.incomeAndExpensesCurrentPage.valueGoodsAndServicesSold).toBe(10000);
    expect(kpir.incomeAndExpensesCurrentPage.totalIncome).toBe(10000);
  });

  it('should update KPiR page when adding an purchase invoice', () => {
    invoice.invoiceType = InvoiceType.sale;
    invoice.net = 10000;
    KpirCalculator.calculateInvoiceCost(invoice);
    KpirCalculator.calculateKpirByAddItem(kpir, invoice);
    let purchaseInvoice: Invoice = new Invoice(null);
    purchaseInvoice.invoiceType = InvoiceType.purchase;
    purchaseInvoice.net = 1000;
    purchaseInvoice.vat = 230;
    purchaseInvoice.vatCost = 50;
    purchaseInvoice.netCost = 75;
    KpirCalculator.calculateInvoiceCost(purchaseInvoice);
    KpirCalculator.calculateKpirByAddItem(kpir, purchaseInvoice);

    expect(kpir.incomeAndExpensesCurrentPage.valueGoodsAndServicesSold).toBe(10000);
    expect(kpir.incomeAndExpensesCurrentPage.totalIncome).toBe(10000);
    expect(kpir.incomeAndExpensesCurrentPage.totalExpenses).toBe(836.25);
    expect(kpir.incomeAndExpensesCurrentPage.otherExpenses).toBe(836.25);
  });

  it('should update KPiR page when deleting an invoice', () => {
    invoice.invoiceType = InvoiceType.sale;
    invoice.net = 10000;
    KpirCalculator.calculateInvoiceCost(invoice);
    KpirCalculator.calculateKpirByAddItem(kpir, invoice);
    KpirCalculator.calculateKpirByDeleteItem(kpir, invoice);

    expect(kpir.incomeAndExpensesCurrentPage.valueGoodsAndServicesSold).toBe(0);
    expect(kpir.incomeAndExpensesCurrentPage.otherIncome).toBe(0);
    expect(kpir.incomeAndExpensesCurrentPage.totalIncome).toBe(0);
    expect(kpir.incomeAndExpensesCurrentPage.payInCash).toBe(0);
    expect(kpir.incomeAndExpensesCurrentPage.otherExpenses).toBe(0);
    expect(kpir.incomeAndExpensesCurrentPage.totalExpenses).toBe(0);
  });
});
