import {KPiR} from "../../entities/invoice/KPiR.model";
import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoiceType} from "../../entities/invoice/InvoiceType";
import {InvoiceIncomeAndExpenses} from "../../entities/invoice/InvoiceIncomeAndExpenses.model";

export class KpirCalculator {
  public static calculateKpirByAddItem(kpir: KPiR, invoice: Invoice) {
    this.updateKpirPage(kpir.incomeAndExpensesCurrentPage, invoice, 'add');
    this.updateKpirPage(kpir.incomeAndExpensesYear, invoice, 'add');
  }

  public static calculateKpirByDeleteItem(kpir: KPiR, invoice: Invoice) {
    this.updateKpirPage(kpir.incomeAndExpensesCurrentPage, invoice, 'delete');
    this.updateKpirPage(kpir.incomeAndExpensesYear, invoice, 'delete');
  }

  public static calculateKpirByEditItem(kpir: KPiR, invoiceDeleted: Invoice, invoiceAdded: Invoice) {
    this.calculateKpirByDeleteItem(kpir, invoiceDeleted);
    this.calculateKpirByAddItem(kpir, invoiceAdded);
  }

  private static updateKpirPage(page: InvoiceIncomeAndExpenses, invoice: Invoice, operation: 'add' | 'delete') {
    const factor = operation === 'add' ? 1 : -1;
    const invoiceCost = invoice.invoiceIncomeAndExpenses;

    page.valueGoodsAndServicesSold += factor * invoiceCost.valueGoodsAndServicesSold;
    page.otherIncome += factor * invoiceCost.otherIncome;
    page.totalIncome += factor * invoiceCost.totalIncome;
    page.payInCash += factor * invoiceCost.payInCash;
    page.otherExpenses += factor * invoiceCost.otherExpenses;
    page.totalExpenses += factor * invoiceCost.totalExpenses;
  }

  public static calculateInvoiceCost(invoice: Invoice) {
    if (invoice.invoiceType === InvoiceType.sale) {
      this.calculateSaleInvoiceCost(invoice);
    } else {
      this.calculatePurchaseInvoiceCost(invoice);
    }
  }

  private static calculateSaleInvoiceCost(invoice: Invoice) {
    const { net, invoiceIncomeAndExpenses } = invoice;
    invoiceIncomeAndExpenses.valueGoodsAndServicesSold = net;
    invoiceIncomeAndExpenses.totalIncome = invoiceIncomeAndExpenses.otherIncome + invoiceIncomeAndExpenses.valueGoodsAndServicesSold;
  }

  private static calculatePurchaseInvoiceCost(invoice: Invoice) {
    const vatPercent = invoice.vatCost * 0.01;
    const totalCostPercent = invoice.netCost * 0.01;
    const { net, vat } = invoice;

    const otherExpenses = +((net + vat * vatPercent) * totalCostPercent).toFixed(2);
    invoice.invoiceIncomeAndExpenses.otherExpenses = otherExpenses;
    invoice.invoiceIncomeAndExpenses.totalExpenses = +(invoice.invoiceIncomeAndExpenses.payInCash + otherExpenses).toFixed(2);
  }
}
