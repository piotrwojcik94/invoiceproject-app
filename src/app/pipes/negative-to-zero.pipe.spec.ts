import { NegativeToZeroPipe } from './negative-to-zero.pipe';

describe('NegativeToZeroPipe', () => {
  let pipe: NegativeToZeroPipe;

  beforeEach(() => {
    pipe = new NegativeToZeroPipe();
  });

  it('should transform negative value to 0', () => {
    expect(pipe.transform(-5)).toBe(0);
  });

  it('should return positive value unchanged', () => {
    expect(pipe.transform(5)).toBe(5);
  });
});
