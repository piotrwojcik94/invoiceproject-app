import {EventEmitter, Injectable, Output} from "@angular/core";
import {Contractor} from "../entities/invoice/Contractor.model";
import {GenerateContractorFromCeidgComponent} from "../tools/GenerateContractorFromCeidg.component";
import {ContractorService} from "../services/contractor.service";
import {AppSettings} from "../app-settings";

@Injectable()
export class HeaderService {
  constructor(private contractorService: ContractorService) {
  }

  contractor: Contractor = new Contractor(null);

  @Output() change: EventEmitter<Contractor> = new EventEmitter();

  getContractorData() {
    let nip = sessionStorage.getItem(AppSettings.USER_NIP);
    if (nip != null) {
      this.contractorService.getContractorFromCeidg(nip).then((data: any) => {
        if (data != null) {
          this.contractor = GenerateContractorFromCeidgComponent.generate(data.result.subject);
          this.change.emit(this.contractor);
        }
      });
    }
  }

}
