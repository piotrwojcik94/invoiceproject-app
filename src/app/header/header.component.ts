import {Component} from '@angular/core';
import {AuthenticationService} from "../services/AuthenticationService.service";
import {ContractorService} from "../services/contractor.service";
import {Contractor} from "../entities/invoice/Contractor.model";
import {HeaderService} from "./header.service";
import {AppSettings} from "../app-settings";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent{

  contractor: Contractor = new Contractor(null);

  constructor(public authenticationService: AuthenticationService,
              private contractorService: ContractorService,
              private headerService: HeaderService) {
    this.headerService.change.subscribe(contractor => {
      this.contractor = contractor;
      localStorage.setItem(AppSettings.CONTRACTOR, JSON.stringify(this.contractor));
    });
  }

  ngOnInit() {
    let contractor: Contractor = JSON.parse(<string>localStorage.getItem(AppSettings.CONTRACTOR));
    if (contractor != null) {
      this.contractor = contractor;
    }
  }

}
