import {BrowserModule, Title} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from "./header/header.component";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { SaleInvoiceEditComponent } from './invoice/invoiceEdit/saleInvoiceEdit/sale-invoice-edit.component';
import { SaleInvoiceListComponent } from './invoice/invoiceList/saleInvoiceList/sale-invoice-list.component';
import { PurchaseInvoiceEditComponent } from './invoice/invoiceEdit/purchaseInvoiceEdit/purchase-invoice-edit.component';
import { PurchaseInvoiceListComponent } from './invoice/invoiceList/purchaseInvoiceList/purchase-invoice-list.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ContractorService} from "./services/contractor.service";
import { DatePipe } from '@angular/common';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {SaleInvoiceItemComponent} from "./invoice/invoiceEdit/newInvoiceItem/saleInvoiceItem/sale-invoice-item.component";
import {PurchaseInvoiceItemComponent} from "./invoice/invoiceEdit/newInvoiceItem/purchaseInvoiceItem/purchase-invoice-item.component";
import {RecordsComponent} from "./vatRecords/records/records.component";
import {RecordsStartComponent} from "./vatRecords/records-start/records-start.component";
import {RegisterUserComponent} from "./registerUser/register-user.component";
import {UserService} from "./services/user.service";
import {LoginUserComponent} from "./loginUser/login-user.component";
import {RecaptchaModule} from "ng-recaptcha";
import {MatSliderModule} from "@angular/material/slider";
import {NIPValidator} from "./validators/NIPValidator";
import {EmailValidator} from "./validators/EmailValidator";
import {PasswordValidator} from "./validators/PasswordValidator";
import {PasswordConfirmValidator} from "./validators/PasswordConfirmValidator";
import {UniqueEmailValidator} from "./validators/UniqueEmailValidator";
import {UniqueNIPValidator} from "./validators/UniqueNIPValidator";
import {HeaderService} from "./header/header.service";
import { KpirComponent } from './kpir/kpir.component';
import { NewKpirItemComponent } from './kpir/new-kpir-item/new-kpir-item.component';
import {InvoiceService} from "./services/invoice.service";
import {KpirService} from "./services/kpir.service";
import { PdfViewerComponent } from './kpir/pdf-viewer/pdf-viewer.component';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {GoogleChartsModule} from "angular-google-charts";
import { StatisticsComponent } from './statistics/statistics.component';
import {NotEmptyValidator} from "./validators/NotEmptyValidator";
import {NumberValidator} from "./validators/NumberVlidator";
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {MatRadioModule} from "@angular/material/radio";
import { TaxAndHealthContributionComponent } from './share/tax-and-health-contribution/tax-and-health-contribution.component';
import { NewKpirItemExpenseComponent } from './kpir/new-kpir-item-expense/new-kpir-item-expense.component';
import { PurchaseNoVatInvoiceItemComponent } from './invoice/invoiceEdit/newInvoiceItem/purchaseNoVatInvoiceItem/purchase-no-vat-invoice-item.component';
import {ServiceLocator} from "./services/ServiceLocator.service";
import {RouterModule} from "@angular/router";
import {MonthNavigationComponent} from "./navigation/month-navigation/month-navigation.component";
import {YearNavigationComponent} from "./navigation/year-navigation/year-navigation.component";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {BaseInvoiceList} from "./invoice/invoiceList/baseInvoiceList/base-invoice-list.component";
import {BaseInvoiceEdit} from "./invoice/invoiceEdit/baseInvoiceEdit/base-invoice-edit";
import {AuthInterceptorService} from "./services/auth-interceptor.service";
import {ContractorListComponent} from "./contractor/contractor-list/contractor-list.component";
import {PageNavigationComponent} from "./navigation/page-navigation/page-navigation.component";
import {ContractorEditComponent} from "./contractor/contractor-edit/contractor-edit.component";
import {NegativeToZeroPipe} from "./pipes/negative-to-zero.pipe";
import {MatAutocomplete, MatAutocompleteTrigger, MatOption} from "@angular/material/autocomplete";
import {ContractorComponent} from "./invoice/contractor-component/contractor.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SaleInvoiceEditComponent,
    SaleInvoiceListComponent,
    PurchaseInvoiceEditComponent,
    PurchaseInvoiceListComponent,
    SaleInvoiceItemComponent,
    PurchaseInvoiceItemComponent,
    RecordsComponent,
    RecordsStartComponent,
    RegisterUserComponent,
    LoginUserComponent,
    NIPValidator,
    EmailValidator,
    PasswordValidator,
    PasswordConfirmValidator,
    UniqueEmailValidator,
    UniqueNIPValidator,
    NotEmptyValidator,
    NumberValidator,
    KpirComponent,
    StatisticsComponent,
    NewKpirItemComponent,
    SaleInvoiceListComponent,
    PurchaseInvoiceListComponent,
    PdfViewerComponent,
    StatisticsComponent,
    HomeComponent,
    UserProfileComponent,
    TaxAndHealthContributionComponent,
    NewKpirItemExpenseComponent,
    PurchaseNoVatInvoiceItemComponent,
    MonthNavigationComponent,
    YearNavigationComponent,
    PageNavigationComponent,
    BaseInvoiceList,
    BaseInvoiceEdit,
    ContractorListComponent,
    ContractorEditComponent,
    NegativeToZeroPipe,
    ContractorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    GoogleChartsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    MatTabsModule,
    MatMomentDateModule,
    MatSliderModule,
    BrowserAnimationsModule,
    RecaptchaModule,
    NgxExtendedPdfViewerModule,
    MatRadioModule,
    RouterModule,
    FontAwesomeModule,
    PdfViewerModule,
    MatAutocomplete,
    MatOption,
    MatAutocompleteTrigger
  ],
  providers: [KpirService, InvoiceService, ContractorService, UserService, HeaderService, DatePipe, Title, ServiceLocator, {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    ServiceLocator.injector = injector;
  }
}
