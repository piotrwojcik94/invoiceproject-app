import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {NewKpirItemComponent} from "./new-kpir-item/new-kpir-item.component";
import {Invoice} from "../entities/invoice/Invoice.model";
import {InvoiceService} from "../services/invoice.service";
import {InvoiceTypeToLabelMapping} from "../entities/invoice/InvoiceType";
import {KPiR} from "../entities/invoice/KPiR.model";
import {KpirService} from "../services/kpir.service";
import 'jspdf-autotable';
import pdfMake from 'pdfmake/build/pdfmake.js';
import pdfFonts from 'pdfmake/build/vfs_fonts.js';
import {KpirColumns} from "../pdf/kpirPdf/KpirColumns";
import {AppSettings} from "../app-settings";
import {Contractor} from '../entities/invoice/Contractor.model';
import {Title} from "@angular/platform-browser";
import {MonthNavigationComponent} from "../navigation/month-navigation/month-navigation.component";
import {PdfViewerComponent} from "./pdf-viewer/pdf-viewer.component";
import {TaxAndHealthContributionComponent} from "../share/tax-and-health-contribution/tax-and-health-contribution.component";
import {KpirCalculator} from "../calculator/kpir-calculator/kpir-calculator";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-kpir',
  templateUrl: './kpir.component.html',
  styleUrls: ['./kpir.component.css']
})
export class KpirComponent implements OnInit {

  invoices: Invoice[] = [];

  kpir: KPiR;

  invoiceTypeMapping = InvoiceTypeToLabelMapping;

  columns : String[] = ["Lp.", "Data Księgowania", "Nr faktury", "Typ faktury", "Nazwa kontrahenta", "NIP", "Netto [PLN]", "VAT [PLN]", "Brutto [PLN]"];

  @ViewChild('childComponent') monthNavigation: MonthNavigationComponent;

  @ViewChild('taxAndHealthContribution') taxAndContribution:TaxAndHealthContributionComponent;

  constructor(private titleService: Title,
              private invoiceService: InvoiceService,
              private kpirService: KpirService,
              private modalService: NgbModal) {
    this.titleService.setTitle("Księga przychodów i rozchodów");
  }

  ngOnInit() {
  }

  getInvoiceAndKPiR() {
    this.kpirService.getKpirAndInvoices(this.monthNavigation.getSelectedDate())?.subscribe((data) => {
      this.kpir = new KPiR(data.kpir, new Date(this.monthNavigation.getSelectedDate().year, this.monthNavigation.getSelectedDate().month-1, 1));
      this.invoices = data.invoices;
    })
  }

  addInvoiceToKpir(i: number) {
    if (this.kpir) {
      this.openInvoiceModal(this.invoices[i], true, i);
    } else {
      alert('Poprzedni miesiąc nie został rozliczony, dodanie faktury jest niemożliwe');
    }
  }

  editKpirItem(i: number) {
    this.openInvoiceModal(this.kpir.invoices[i], false, i);
  }

  private openInvoiceModal(invoice: Invoice, isNew: boolean, i: number): void {
    const modal = this.modalService.open(NewKpirItemComponent);
    modal.componentInstance.invoice = {...invoice};

    modal.componentInstance.emitData.subscribe(($event: any) => {
      let updatedInvoice: Invoice = JSON.parse(JSON.stringify($event.invoice));
      KpirCalculator.calculateInvoiceCost(updatedInvoice);
      if (isNew) {
        updatedInvoice.isCost = true;
        this.invoices.splice(i, 1);
        KpirCalculator.calculateKpirByAddItem(this.kpir, updatedInvoice);
        this.kpir.invoices.push(updatedInvoice);
      } else {
        KpirCalculator.calculateKpirByEditItem(this.kpir, this.kpir.invoices[i], updatedInvoice);
        this.kpir.invoices[i] = updatedInvoice;
      }

      this.taxAndContribution.calculateAndSaveMonthSummary();
    });
  }

  deleteKpirItem(i: number) {
    if(confirm("Czy na pewno chcesz usunąć pozycje z KPiR?")) {
      let invoice: Invoice = this.kpir.invoices[i];
      this.invoices.push(invoice);
      invoice.isCost = false;
      invoice.kpir = new KPiR(null, new Date());
      KpirCalculator.calculateKpirByDeleteItem(this.kpir, invoice);
      this.kpir.invoices.splice(i, 1);
      this.taxAndContribution.calculateAndSaveMonthSummary();
    }
  }

  viewPdfInModal(pdf: any) {
    const modal = this.modalService.open(PdfViewerComponent, {size: 'xl'});
    modal.componentInstance.src = pdf;
    modal.componentInstance.name = this.getPdfFileName();
  }

  viewPdf() {
    let body: any = [];
    KpirColumns.setDataOnPDF(body, KpirColumns.header);
    KpirColumns.getData(body, this.kpir);
    KpirColumns.setDataOnPDF(body, KpirColumns.getFooter(this.kpir));

    const documentDefinition = {
      fontSize: 15, pageMargins: [ 10,40,10,40 ], pageOrientation: 'landscape', padding: 0, content: [
        {columns: [
          {
            width: 200,
            text: 'Księga przychodów i rozchodów\n'+this.monthNavigation.toString(),
            margin: [5,5,5,5]
          },
          {
            text: this.getContractorData(),
            margin: [5,5,5,5]
          }
          ]},
          {
          alignment: 'center',
          style: ['small'],
            padding: 0,
          color: '#555',
          table: {
            margin: [0,0,0,0],
            widths: ['auto', 'auto', '7%', '11%', '11%', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
            body: body,
          }
        },
      ],
      styles: {
        small: {
          fontSize: 9
        },
        headerStyle: {
          bold: true,
          fillColor: '#C0C0C0',
          color: '#000000',
          alignment: 'center'
        },
        rowStyle : {
          bold: true,
          color: '#000000',
          alignment: 'center'
        },
        countCell : {
          alignment: 'left', fillColor: '#C0C0C0', color: '#000000', bold: true
        },
        data1Cell : {
          alignment: 'left', color: '#000000', bold: true
        },
        data2Cell :{
          alignment: 'center', color: '#000000', bold: true
        },
        data3Cell : {
          alignment: 'center', color: '#000000', fontSize: 6
        }
      }};
    //@ts-ignore
    pdfMake.createPdf(documentDefinition).getBuffer((arrayBuffer) => {
        this.viewPdfInModal(arrayBuffer);
      });
  }

  getContractorData() {
    let contr = new Contractor(JSON.parse(<string>localStorage.getItem(AppSettings.CONTRACTOR)));
    return contr.getContractorData();
  }

  getPdfFileName() {
    let contr = new Contractor(JSON.parse(<string>localStorage.getItem(AppSettings.CONTRACTOR)));
    let pdfName = contr.name + ' ' + this.monthNavigation.toString();
    return pdfName.replace(/ /g, '_');
  }
}
