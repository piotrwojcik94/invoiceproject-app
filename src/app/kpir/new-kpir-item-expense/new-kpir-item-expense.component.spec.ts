import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewKpirItemExpenseComponent } from './new-kpir-item-expense.component';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";

describe('NewKpirItemExpenseComponent', () => {
  let component: NewKpirItemExpenseComponent;
  let fixture: ComponentFixture<NewKpirItemExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewKpirItemExpenseComponent ],
      providers: [NgbActiveModal],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewKpirItemExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
