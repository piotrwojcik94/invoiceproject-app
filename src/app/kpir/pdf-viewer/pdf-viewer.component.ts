import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {PDFDocumentProxy} from "ng2-pdf-viewer";

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.css']
})
export class PdfViewerComponent implements OnInit {

  //@ts-ignore
  src;
  name: string = '';
  isPdfLoaded = false;
  private pdf: PDFDocumentProxy;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onLoaded(pdf: PDFDocumentProxy) {
    this.pdf = pdf;
    this.isPdfLoaded = true;
  }

  download() {
    this.pdf.getData().then((u8) => {
      let blob = new Blob([u8.buffer], {
        type: 'application/pdf'
      });
      let blobURL = URL.createObjectURL(blob);
      let anchor = document.createElement("a");
      anchor.download = this.name+'.pdf';
      anchor.href = blobURL;
      anchor.click();
    });
  }

  print() {
    //TO DO nothing is printed
    /*this.pdf.getData().then((u8) => {
      let blob = new Blob([u8.buffer], {
        type: 'application/pdf'
      });

      const blobUrl = window.URL.createObjectURL((blob));
      const iframe = document.createElement('iframe');
      iframe.style.display = 'none';
      iframe.src = blobUrl;
      document.body.appendChild(iframe);
      iframe.contentWindow?.print();
    });*/
  }

}
