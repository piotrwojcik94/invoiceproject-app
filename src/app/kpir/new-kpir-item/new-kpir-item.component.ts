import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Invoice} from "../../entities/invoice/Invoice.model";

@Component({
  selector: 'new-kpir-item',
  templateUrl: './new-kpir-item.component.html',
  styleUrls: ['./new-kpir-item.component.css']
})
export class NewKpirItemComponent implements OnInit {

  @Output() emitData: EventEmitter<any> = new EventEmitter();

  invoice: Invoice = new Invoice(null);

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.emitData.emit({invoice: this.invoice});
    this.activeModal.close();
  }

}
