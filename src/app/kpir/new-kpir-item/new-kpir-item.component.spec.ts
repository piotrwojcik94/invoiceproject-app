import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewKpirItemComponent } from './new-kpir-item.component';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";

describe('NewKpirItemComponent', () => {
  let component: NewKpirItemComponent;
  let fixture: ComponentFixture<NewKpirItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewKpirItemComponent ],
      providers: [NgbActiveModal],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewKpirItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
