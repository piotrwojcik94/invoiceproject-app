import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockComponent } from 'ng-mocks';
import { KpirComponent } from './kpir.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {InvoiceService} from "../services/invoice.service";
import {MonthNavigationComponent} from "../navigation/month-navigation/month-navigation.component";
import {TaxAndHealthContributionComponent} from "../share/tax-and-health-contribution/tax-and-health-contribution.component";
import {Title} from "@angular/platform-browser";

describe('KpirComponent', () => {
  let titleService: Title;
  let component: KpirComponent;
  let fixture: ComponentFixture<KpirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [KpirComponent, MockComponent(MonthNavigationComponent), MockComponent(TaxAndHealthContributionComponent)],
      providers: [InvoiceService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have title as 'Księga przychodów i rozchodów'`, () => {
    titleService = TestBed.get(Title);
    expect(titleService.getTitle()).toBe('Księga przychodów i rozchodów');
  });
});
