import {PriceType} from "../entities/invoice/PriceType";
import {FormGroup} from "@angular/forms";
import {InvoiceItem} from "../entities/invoice/InvoiceItem.model";

export class SetDataOnInvoiceItemForm {

  setPurchaseItem(form: FormGroup, item: InvoiceItem, priceType: PriceType) {
    this.setCommonFieldItems(form, item, priceType);
    form.controls['deduct50'].setValue(item.deduct50);
    form.controls['taxationReason'].setValue(item.taxationReason);
  }

  setPurchaseNoVatItem(form: FormGroup, item: InvoiceItem) {
    form.controls['name'].setValue(item.name);
    form.controls['price'].setValue(item.priceNet);
    form.controls['amount'].setValue(item.amount);
    form.controls['unit'].setValue(item.unit);
    form.controls['taxationReason'].setValue(item.taxationReason);
  }

  setSaleItem(form: FormGroup, item: InvoiceItem, priceType: PriceType) {
    this.setCommonFieldItems(form, item, priceType);
    form.controls['description'].setValue(item.description);
    form.controls['vat'].setValue(item.vat);
  }

  private setCommonFieldItems(form: FormGroup, item: InvoiceItem, priceType: PriceType) {
    form.controls['name'].setValue(item.name);
    if(priceType == PriceType.net) {
      form.controls['price'].setValue(item.priceNet);
    } else {
      form.controls['price'].setValue(item.priceGross);
    }
    form.controls['amount'].setValue(item.amount);
    form.controls['unit'].setValue(item.unit);
    form.controls['vat'].setValue(item.vat);
  }
}
