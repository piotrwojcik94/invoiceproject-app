export class ConvertNumberToWord {

  static units: any = ['', 'jeden ', 'dwa ', 'trzy ', 'cztery ', 'pięć ', 'sześć ', 'siedem ', 'osiem ', 'dziewięć '];
  static tens: any = ['', 'jedenaście ', 'dwanaście ', 'trzynaście ', 'czternaście ', 'piętnaście ', 'szesnaście ', 'siedemnaście ', 'osiemnaście ', 'dziewiętnaście '];
  static dozens: any = ['', 'dziesięć ', 'dwadzieścia ', 'trzydzieści ', 'czterdzieści ', 'pięćdziesiąt ', 'sześćdziesiąt ', 'siedemdziesiąt ', 'osiemdziesiąt ', 'dziewięćdziesiąt '];
  static hundreds: any = ['', 'sto ', 'dwieście ', 'trzysta ', 'czterysta ', 'pięćset ', 'sześćset ', 'siedemset ', 'osiemset ', 'dziewięćset '];
  static groups: any = [
    ['', '', ''],
    ['tysiąc ', 'tysiące ', 'tysięcy '],
    [' milion ',' miliony ' ,' milionów '],
    [' miliard ',' miliardy ',' miliardów '],
    [' bilion ' ,' biliony ',' bilionów '],
    [' biliard ',' biliardy ',' biliardów '],
    [' trylion ',' tryliony ',' trylionów ']
  ];

  public static convert(sum: number) {
    let result = '';

    let integer = ~~sum;
    let decimal = Math.round((sum - integer) *100);

    result += this.calculate(~~integer) + 'złotych ';
    result += this.calculate(~~decimal) + 'groszy';

    return result;
}

  static calculate(number: number) {
    let result = '';

    let g = 0;
    while (number > 0) {
      let s = Math.floor((number % 1000)/100);
      let n = 0;
      let d = Math.floor((number % 100)/10);
      let j = Math.floor(number % 10);

      if (d == 1 && j>0) {
        n = j;
        d = 0;
        j = 0;
      }

      let k = 2;
      if (j == 1 && s+d+n == 0)
        k = 0;
      if (j == 2 || j == 3 || j == 4)
        k = 1;
      if (s+d+n+j > 0)
        result = this.hundreds[s]+this.dozens[d]+this.tens[n]+this.units[j]+this.groups[g][k]+result;

      g++;
      number = Math.floor(number/1000);
    }

    return result;
  }
}
