import {Contractor} from "../entities/invoice/Contractor.model";
import {ContractorCeidg} from "../entities/invoice/ContractorCeidg.model";

export class GenerateContractorFromCeidgComponent{

  static generate(contractorCeidg: ContractorCeidg) {
    let contractor: Contractor = new Contractor(null);
    contractor.name = contractorCeidg.name;
    contractor.nip = contractorCeidg.nip;
    contractor.regon = contractorCeidg.regon;
    contractor.createDate = contractorCeidg.registrationLegalDate;
    let address : string;
    if (contractorCeidg.residenceAddress == null) {
      address = contractorCeidg.workingAddress;
    } else {
      address = contractorCeidg.residenceAddress;
    }

    let streetAndHouseAndFlat = address.split(',')[0].split(' ');
    let zipCodeAndCity = address.split(',')[1].split(' ');

    contractor.zipCode = zipCodeAndCity[1];
    let city = '';
    for (let i = 2; i < zipCodeAndCity.length; i++) {
      city += zipCodeAndCity[i]+' ';
    }
    contractor.city = city;

    let houseNumberAndFlat = streetAndHouseAndFlat[streetAndHouseAndFlat.length-1];
    let street = '';
    for (let i = 0; i < streetAndHouseAndFlat.length-1; i++) {
      street+=streetAndHouseAndFlat[i]+' ';
    }
    contractor.street = street;
    contractor.houseNumber = houseNumberAndFlat.split('/')[0];
    contractor.flatNumber = houseNumberAndFlat.split('/')[1];
    return contractor;
  }
}
