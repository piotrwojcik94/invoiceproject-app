export class CalculateDateComponent {
  public static calculateDaysBetween2Dates(date1: Date, date2: Date) {
    return Math.ceil(Math.abs(date1.getTime() - date2.getTime()) / (1000 * 3600 * 24));
  }

  public static getFirstDayOfMonthDate(date: Date) {
    date = new Date(date);
    let firstDayDate = new Date(date.getFullYear(), date.getMonth(), 1);
    return this.getFormattedDate(firstDayDate);
  }

  public static getLastDayOfMonthDate(date: Date) {
    date = new Date(date);
    let lastDayDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    return this.getFormattedDate(lastDayDate);
  }

  public static getFormattedDate(date: Date) {
    date = new Date(date);
    let month = '' + (date.getMonth() + 1);
    let day = '' + date.getDate();
    let year = date.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }
}
