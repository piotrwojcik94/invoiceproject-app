import {UploadFileService} from "../services/uploadService";
import {PdfViewerComponent} from "../kpir/pdf-viewer/pdf-viewer.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

export class DocumentOpener {

  constructor(private uploadService: UploadFileService,
              private modalService: NgbModal) {  }

  getDocumentAndOpen(documentId: number) {
    this.uploadService.getPdf(documentId)?.subscribe((data: any) => {
      this.openDocument(data);
    });
  }

  openDocument(document: any) {
    let blob = new Blob([document], {type: 'application/pdf'});
    const modal = this.modalService.open(PdfViewerComponent, {size: 'xl'});
    new Response(blob).arrayBuffer().then((arrayBuffer) => {
      modal.componentInstance.src = arrayBuffer;
    });

    //modal.componentInstance.name = this.getPdfFileName();
  }
}
