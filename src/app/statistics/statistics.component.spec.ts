import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsComponent } from './statistics.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MockComponent} from "ng-mocks";
import {YearNavigationComponent} from "../navigation/year-navigation/year-navigation.component";
import {GoogleChartsModule} from "angular-google-charts";

describe('StatisticsComponent', () => {
  let component: StatisticsComponent;
  let fixture: ComponentFixture<StatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, GoogleChartsModule],
      declarations: [ StatisticsComponent, MockComponent(YearNavigationComponent) ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
