import {Component, OnInit, ViewChild} from '@angular/core';
import {StatisticService} from "../services/statistic.service";
import {ChartTypeDescription, ChartTypeToLabelMapping} from "../entities/invoice/ChartTypeDescription";
import {Title} from "@angular/platform-browser";
import {ChartType} from "angular-google-charts";
import {YearNavigationComponent} from "../navigation/year-navigation/year-navigation.component";
import {Row} from "angular-google-charts";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  title: string = '';
  type: ChartType = ChartType.ColumnChart;
  data: Row[];
  columnNames :string[] = [];
  options = {};
  width = 1050;
  height = 600;

  @ViewChild('childComponent') monthNavigation: YearNavigationComponent;

  constructor(private titleService: Title,
              private statisticService: StatisticService) {
    this.titleService.setTitle("Statystyki");
  }

  ngOnInit() {
    //this.getInvoicesStatistics();
  }

  onYearChange() {
    this.getInvoicesStatistics();
  }

  getInvoicesStatistics() {
    if (this.monthNavigation.selectedChartType == ChartTypeToLabelMapping[ChartTypeDescription.income_and_expense_monthly]) {
      this.statisticService.getIncomeAndExpense(this.monthNavigation.selectedDate.year)?.subscribe((data: any) => {
        this.setData(data);
      });
    } else if(this.monthNavigation.selectedChartType == ChartTypeToLabelMapping[ChartTypeDescription.income_and_expense_monthly_ascending]) {
      this.statisticService.getIncomeAndExpenseAscending(this.monthNavigation.selectedDate.year)?.subscribe((data: any) => {
        this.setData(data);
      });
    } else if(this.monthNavigation.selectedChartType == ChartTypeToLabelMapping[ChartTypeDescription.vat_monthly]) {
      this.statisticService.getVatMonthly(this.monthNavigation.selectedDate.year)?.subscribe((data: any) => {
        this.setDataVat(data);
      });
    } else if(this.monthNavigation.selectedChartType == ChartTypeToLabelMapping[ChartTypeDescription.vat_monthly_ascending]) {
      this.statisticService.getVatMonthlyAscending(this.monthNavigation.selectedDate.year)?.subscribe((data : any) => {
       this.setDataVat(data);
      });
    }
  }

  setData(data : string[]) {
    this.data = [];
    this.title = this.monthNavigation.selectedChartType;
    this.columnNames = ['Miesiąc', 'Przychody','Koszty'];
    Object.keys(data).forEach( (key:string) => {
      //@ts-ignore
      let value: string = data[key];
      //@ts-ignore
      this.data.push([key, value[0], value[1]]);
    });
  }

  setDataVat(data : string[]) {
    this.data = [];
    this.title = this.monthNavigation.selectedChartType;
    this.columnNames = ['Miesiąc', 'Vat należny','Vat naliczony', 'Vat do zapłaty'];
    Object.keys(data).forEach( (key:string) => {
      //@ts-ignore
      let value : string = data[key];
      //@ts-ignore
      this.data.push([key, value[0], value[1], value[2]]);
    });
  }

}
