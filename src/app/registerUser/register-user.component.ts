import {Component, OnInit} from "@angular/core";
import {UserService} from "../services/user.service";
import {User} from "../entities/invoice/User.model";
import {LoginService} from "../services/login.service";
import {FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {NIPValidator} from "../validators/NIPValidator";
import {EmailValidator} from "../validators/EmailValidator";
import {PasswordValidator} from "../validators/PasswordValidator";
import {PasswordConfirmValidator} from "../validators/PasswordConfirmValidator";
import {Title} from "@angular/platform-browser";
declare var grecaptcha: any;

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css'],
})
export class RegisterUserComponent implements OnInit {

  registerForm: FormGroup;
  nip: FormControl = new FormControl('', [NIPValidator.validateNIP as ValidatorFn]);
  email: FormControl = new FormControl('', [EmailValidator.validateEmail as ValidatorFn]);
  password: FormControl = new FormControl('', [PasswordValidator.validatePassword as ValidatorFn]);
  passwordConfirm: FormControl = new FormControl('');
  hide = true;
  captchaInvalid: boolean = true;

  constructor(private userService: UserService,
              private loginService: LoginService,
              private titleService: Title) {
    this.titleService.setTitle('Zarejestruj się');
    this.registerForm = new FormGroup({
      email: this.email,
      nip: this.nip,
      password: this.password,
      passwordConfirm: this.passwordConfirm
    }, {validators: PasswordConfirmValidator.validatePasswordConfirm as ValidatorFn});
  }

  ngOnInit() {
  }

  saveUser() {
    let userName = this.registerForm.get('email')?.value;
    let userPassword = this.registerForm.get('password')?.value;
    let userNIP = this.registerForm.get('nip')?.value;
    let user: User = new User(userName, userPassword, userNIP);

    this.userService.saveUser(user).subscribe(data => {
      this.loginService.authenticate({username: userName, password: userPassword}, () => {});
      }, error => console.log(error));
  }

  resolved(captchaResponse: string|null) {
    this.captchaInvalid = captchaResponse == null;
  }

  onPasswordChange() {
    if (this.passwordConfirm.value.length != 0 && this.passwordConfirm.value != this.password.value) {
      this.passwordConfirm.setErrors({passwordMismatch: 'Hasła nie pasują'});
    } else {
      this.passwordConfirm.setErrors(null);
    }
  }

  onPasswordConfirmChange() {
    if(this.registerForm.hasError('passwordMismatch')) {
      this.passwordConfirm.setErrors({passwordMismatch: 'Hasła nie pasują'});
    } else {
      this.passwordConfirm.setErrors(null);
    }
  }

}
