import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RegisterUserComponent} from "./register-user.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HeaderService} from "../header/header.service";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {RecaptchaModule} from "ng-recaptcha";
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('register-user', () => {
  let component: RegisterUserComponent;
  let fixture: ComponentFixture<RegisterUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatInputModule, MatIconModule, RecaptchaModule, ReactiveFormsModule, BrowserAnimationsModule],
      declarations: [ RegisterUserComponent ],
      providers: [HeaderService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('Title for registration', () => {
  //   const fixture = TestBed.createComponent(RegisterUserComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h2').textContent).toContain('ZAREJESTRUJ UŻYTKOWNIKA');
  // });

  // it('Error for email validation at start', () => {
  //   const fixture = TestBed.createComponent(RegisterUserComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   let el = compiled.querySelector('#email');
  //   let el1 = compiled.querySelector(By.directive(MatError));
  //   el.value = '1';
  //   const errors = fixture.debugElement.queryAll(By.css("mat-error"));
  //   expect(errors[0].nativeElement.innerHTML.trim().value).toBe('email');
  // });

});
