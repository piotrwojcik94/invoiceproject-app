import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YearNavigationComponent } from './year-navigation.component';
import {MockProviders} from "ng-mocks";
import {BaseNavigationComponent} from "../../BaseNavigationComponent";
import {FormsModule} from "@angular/forms";
import {SelectedDate} from "../../entities/invoice/SelectedDate.model";

describe('YearNavigationComponent', () => {
  let component: YearNavigationComponent;
  let fixture: ComponentFixture<YearNavigationComponent>;
  let store: { [key: string]: string } = {};

  beforeEach(async () => {
    store = {contractor: JSON.stringify({createDate: new Date("Wen Aug 1,2018")})}
    spyOn(localStorage, 'getItem').and.callFake((key: string): string | null => {
      return store[key] || null;
    });
    await TestBed.configureTestingModule({
      declarations: [YearNavigationComponent],
      providers: [MockProviders(BaseNavigationComponent,SelectedDate)],
      imports: [FormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(YearNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
