import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ChartTypeDescription, ChartTypeToLabelMapping} from "../../entities/invoice/ChartTypeDescription";
import {BaseNavigationComponent} from "../../BaseNavigationComponent";

@Component({
  selector: 'app-year-navigation',
  templateUrl: './year-navigation.component.html',
  styleUrl: './year-navigation.component.css'
})
export class YearNavigationComponent extends BaseNavigationComponent implements OnInit ,AfterViewInit {
  chartTypes: string[] = [];
  selectedChartType: string = ChartTypeToLabelMapping[ChartTypeDescription.income_and_expense_monthly];

  @Output() onYearChangeEvent = new EventEmitter<any>();
  dateChangeEvent() {
    this.onYearChangeEvent.emit();
  }

  ngAfterViewInit(): void {
    this.dateChangeEvent();
  }

  ngOnInit(): void {
    this.yearOptions = this.getOptions(this.createdEconomicDate.year, this.currentYear);
    this.chartTypes = this.getChartTypes();
  }

  onYearChange() {
    this.dateChangeEvent();
  }

  getChartTypes() {
    let types: string[] = [];
    for(let chartType of Object.values(ChartTypeDescription)) {
      types.push(ChartTypeToLabelMapping[chartType]);
    }
    return types;
  }
}
