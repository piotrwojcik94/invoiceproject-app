import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-page-navigation',
  templateUrl: './page-navigation.component.html',
  styleUrl: './page-navigation.component.css'
})
export class PageNavigationComponent {
  currentPage: number = 1;
  @Input()
  totalPages: number = 1;
  size: number;

  @Output() onPageChange = new EventEmitter<any>();
  pageChangedEvent() {
    this.onPageChange.emit(this.currentPage-1);
  }

  getPages() {
    return Array.from({length: this.totalPages}, (_, i) => i + 1);
  }

  setCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
    this.pageChangedEvent();
  }

  next() {
    this.currentPage++;
    this.pageChangedEvent();
  }

  prev() {
    this.currentPage--;
    this.pageChangedEvent();
  }
}
