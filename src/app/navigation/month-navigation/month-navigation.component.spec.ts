import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthNavigationComponent } from './month-navigation.component';
import {BaseNavigationComponent} from "../../BaseNavigationComponent";
import {MockProviders} from "ng-mocks";
import {FormsModule} from "@angular/forms";
import {By} from "@angular/platform-browser";
import {SelectedDate} from "../../entities/invoice/SelectedDate.model";
import {SelectedDateFromMonthAndYear} from "../../entities/SelectedDateFromMonthAndYear.model";

describe('MonthNavigationComponent', () => {
  let component: MonthNavigationComponent;
  let fixture: ComponentFixture<MonthNavigationComponent>;
  let store: { [key: string]: string } = {};

  beforeEach(async () => {
    store = {contractor: JSON.stringify({createDate: new Date("Wen Aug 1,2018")})}
    spyOn(localStorage, 'getItem').and.callFake((key: string): string | null => {
      return store[key] || null;
    });
    await TestBed.configureTestingModule({
      declarations: [MonthNavigationComponent],
      providers: [MockProviders(BaseNavigationComponent,SelectedDate)],
      imports: [FormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonthNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('selected date should be equal to current', () => {
    let currentDate = new Date();
    expect(component.selectedDate.month).toEqual(currentDate.getMonth() + 1);
    expect(component.selectedDate.year).toEqual(currentDate.getFullYear());
  });

  it('next button should be disabled on init', () => {
    const nextButton = fixture.debugElement.query(By.css('#nextMonth'));
    expect(nextButton.nativeElement.disabled).toBeTrue();
  });

  it('nextButton should be enabled when prevMonth button is clicked', () => {
    const prevButton = fixture.debugElement.query(By.css('#prevMonth'));
    prevButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(prevButton.nativeElement.disabled).toBeFalse();
  });

  it('prev button should be enabled on init', () => {
    const prevButton = fixture.debugElement.query(By.css('#prevMonth'));
    expect(prevButton.nativeElement.disabled).toBeFalse();
  });

  it('prev button should be disabled date is set to create company', () => {
    component.selectedDate = new SelectedDateFromMonthAndYear(8, 2018);
    const prevButton = fixture.debugElement.query(By.css('#prevMonth'));
    fixture.detectChanges();

    expect(prevButton.nativeElement.disabled).toBeTrue();
  });

  it('should decrement selectedMonth when prevMonth button is clicked', () => {
    component.selectedDate = new SelectedDateFromMonthAndYear(2, 2024);
    const prevButton = fixture.debugElement.query(By.css('#prevMonth'));
    prevButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.selectedDate).toEqual(new SelectedDateFromMonthAndYear(1, 2024));
  });

  it('should decrement selectedMonth and selectedYear when prevMonth button is clicked', () => {
    component.selectedDate = new SelectedDateFromMonthAndYear(1, 2024);
    const prevButton = fixture.debugElement.query(By.css('#prevMonth'));
    prevButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.selectedDate).toEqual(new SelectedDateFromMonthAndYear(12, 2023));
  });

  it('should increment selectedMonth when nextMonth button is clicked', () => {
    component.selectedDate = new SelectedDateFromMonthAndYear(2, 2024);
    const nextButton = fixture.debugElement.query(By.css('#nextMonth'));
    nextButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.selectedDate).toEqual(new SelectedDateFromMonthAndYear(3, 2024));
  });

  it('should increment selectedMonth and selectedYear when nextMonth button is clicked', () => {
    component.selectedDate = new SelectedDateFromMonthAndYear(12, 2023);
    const nextButton = fixture.debugElement.query(By.css('#nextMonth'));
    nextButton.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.selectedDate).toEqual(new SelectedDateFromMonthAndYear(1, 2024));
  });

  it('prev and next buttons should be disabled when create company date is equal to current date', () => {
    store = {contractor: JSON.stringify({createDate: new Date()})}
    fixture = TestBed.createComponent(MonthNavigationComponent);
    fixture.detectChanges();
    const nextButton = fixture.debugElement.query(By.css('#nextMonth'));
    const prevButton = fixture.debugElement.query(By.css('#prevMonth'));

    expect(nextButton.nativeElement.disabled).toBeTrue();
    expect(prevButton.nativeElement.disabled).toBeTrue();
  });



  // it('date of created economic should be Aug-2018', () => {
  //   expect(localStorage.getItem).toHaveBeenCalledWith(AppSettings.CONTRACTOR);
  //   expect(component.monthOfCreatedEconomicActivity).toEqual(8);
  //   expect(component.yearOfCreatedEconomicActivity).toEqual(2018);
  // });
});

