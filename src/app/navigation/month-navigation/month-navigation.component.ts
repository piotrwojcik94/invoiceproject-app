import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BaseNavigationComponent} from "../../BaseNavigationComponent";
import {SelectedDateFromMonthAndYear} from "../../entities/SelectedDateFromMonthAndYear.model";

@Component({
  selector: 'app-month-navigation',
  templateUrl: './month-navigation.component.html',
  styleUrl: './month-navigation.component.css'
})
export class MonthNavigationComponent extends BaseNavigationComponent implements OnInit, AfterViewInit {
  monthOptions: number[];

  @Output() onDateChangeEvent = new EventEmitter<any>();
  dateChangeEvent() {
    this.onDateChangeEvent.emit();
  }

  ngOnInit() {
    this.setCreateDate();
    this.setMonthsOptions();
    this.yearOptions = this.getOptions(this.createdEconomicDate.year, this.currentYear);
  }

  ngAfterViewInit() {
    this.dateChangeEvent();
  }

  onYearChange() {
    this.setMonthsOptions();
    this.selectedDate.month = this.monthOptions[0];
    this.dateChangeEvent();
  }

  setMonthsOptions() {
    if (this.selectedDate.year == this.createdEconomicDate.year) {
      if (this.createdEconomicDate.year == this.currentYear) {
        this.monthOptions = this.getOptions(this.createdEconomicDate.month, this.currentMonth);
      } else {
        this.monthOptions = this.getOptions(this.createdEconomicDate.month, this.MONTHS_IN_YEAR);
      }
    } else if (this.selectedDate.year == new Date().getFullYear()) {
      this.monthOptions = this.getOptions(1, this.currentMonth);
    } else {
      this.monthOptions = this.getOptions(1, this.MONTHS_IN_YEAR);
    }
  }

  onMonthChange() {
    this.dateChangeEvent();
  }

  setNextMonth() {
    if (this.selectedDate.month === this.MONTHS_IN_YEAR) {
      this.selectedDate.year++;
      this.setMonthsOptions();
      this.selectedDate.month = 1;
    } else {
      this.selectedDate.month++;
    }
    this.dateChangeEvent();
  }

  setPreviousMonth() {
    if (this.selectedDate.month == 1) {
      this.selectedDate.year--;
      this.selectedDate.month = this.MONTHS_IN_YEAR;
      this.setMonthsOptions();
    } else {
      this.selectedDate.month--;
    }
    this.dateChangeEvent();
  }

  isPreviousMonthEnable() {
    return !(this.selectedDate.year == this.createdEconomicDate.year
      && this.selectedDate.month == this.createdEconomicDate.month);
  }

  isNextMonthEnable() {
    return !(this.selectedDate.year == this.currentYear
      && +this.selectedDate.month == this.currentMonth);
  }

  getSelectedDate() {
    return new SelectedDateFromMonthAndYear(this.selectedDate.month, this.selectedDate.year);
  }

  //@ts-ignore
  public toString(): string {
    let s;
    switch(this.selectedDate.month) {
      case 1: s = 'Styczeń';break;
      case 2: s = 'Luty';break;
      case 3: s = 'Marzec';break;
      case 4: s = 'Kwiecień';break;
      case 5: s = 'Maj';break;
      case 6: s = 'Czerwiec';break;
      case 7: s = 'Lipiec';break;
      case 8: s = 'Sierpień';break;
      case 9: s = 'Wrzesień';break;
      case 10: s = 'Październik';break;
      case 11: s = 'Listopad';break;
      case 12: s = 'Grudzień';break;
    }

    return s + ' ' + this.selectedDate.year;
  }
}
