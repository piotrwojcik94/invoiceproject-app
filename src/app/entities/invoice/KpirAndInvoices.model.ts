import {KPiR} from "./KPiR.model";
import {Invoice} from "./Invoice.model";

export class KpirAndInvoicesModel {
  kpir: KPiR;
  invoices: Invoice[];
  constructor(data: any) {
    if (data!=null) {
      this.kpir = data.kpir;
      this.invoices = data.invoices;
    }
  }
}
