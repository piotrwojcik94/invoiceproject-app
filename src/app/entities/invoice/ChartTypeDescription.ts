export enum ChartTypeDescription {
  income_and_expense_monthly = "income_and_expense_monthly",
  income_and_expense_monthly_ascending = "income_and_expense_monthly_ascending",
  vat_monthly = "vat_monthly",
  vat_monthly_ascending = "vat_monthly_ascending"
}

export const ChartTypeToLabelMapping: Record<ChartTypeDescription, string> = {
  [ChartTypeDescription.income_and_expense_monthly]: "Przychody i koszty miesięcznie",
  [ChartTypeDescription.income_and_expense_monthly_ascending]: "Przychody i koszty miesięcznie rosnąco",
  [ChartTypeDescription.vat_monthly]: "VAT miesięcznie",
  [ChartTypeDescription.vat_monthly_ascending]: "VAT miesięcznie rosnąco"
};
