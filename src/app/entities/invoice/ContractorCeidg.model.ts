export class ContractorCeidg {
  name: string = '';
  nip: string = '';
  regon: string = '';
  residenceAddress: string = '';
  workingAddress: string = '';
  registrationLegalDate: Date = new Date();
  result: any;

  constructor(data: any) {
    if (data) {
      this.name = data.name;
      this.nip = data.nip;
      this.regon = data.regon;
      this.residenceAddress = data.residenceAddress;
      this.workingAddress = data.workingAddress;
      this.registrationLegalDate = data.registrationLegalDate;
    }
  }
}
