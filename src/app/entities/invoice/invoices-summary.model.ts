export class InvoicesSummary {
  net: number = 0.0;
  vat: number = 0.0;
  gross: number = 0.0;
}
