import {TaxationFormEnum} from "./TaxationFormEnum";

export class TaxationForm {
  taxationForm: TaxationFormEnum = TaxationFormEnum.lump_sum;
  taxationFormYear: number = 0;
  lumpSum: number = 0;
  constructor(taxationFormYear: number) {
    this.taxationFormYear = taxationFormYear;
  }
}
