export enum PriceType {
  net = "net",
  gross = "gross"
}

export const PriceTypeToLabelMapping: Record<PriceType, string> = {
  [PriceType.net]: "Netto",
  [PriceType.gross]: "Brutto"
};
