import {Invoice} from "./Invoice.model";
import {InvoiceIncomeAndExpenses} from "./InvoiceIncomeAndExpenses.model";

export class KPiR {
  recid: number = 0;
  date: Date = new Date();
  invoices: Invoice[] = new Array<Invoice>();
  incomeAndExpensesCurrentPage: InvoiceIncomeAndExpenses = new InvoiceIncomeAndExpenses();
  incomeAndExpensesPreviousPage: InvoiceIncomeAndExpenses = new InvoiceIncomeAndExpenses();
  incomeAndExpensesYear: InvoiceIncomeAndExpenses = new InvoiceIncomeAndExpenses();
  taxToPay: number = 0;

  constructor(data: any, date: Date) {
    if (data != null) {
      this.recid = data.recid;
      this.date = new Date(data.date);
      let invoices = new Array<Invoice>();
      for (let invoice of data.invoices) {
        invoices.push(new Invoice(invoice))
      }
      this.invoices = invoices;
      this.incomeAndExpensesCurrentPage = data.incomeAndExpensesCurrentPage;
      this.incomeAndExpensesPreviousPage = data.incomeAndExpensesPreviousPage;
      this.incomeAndExpensesYear = data.incomeAndExpensesYear;
      this.taxToPay = data.taxToPay;
    } else {
      this.date = date;
    }
  }
}
