export enum TaxationReason {
  other_goods_and_services = "other_goods_and_services",
  permanently_resources = "permanently_resources"
}

export const TaxationReasonToLabelMapping: Record<TaxationReason, string> = {
  [TaxationReason.other_goods_and_services]: "Nabycie pozostałych towarów i usług",
  [TaxationReason.permanently_resources]: "Nabycie środków trwałych"
};
