import {TaxationReason} from "./TaxationReason";
import {Invoice} from "./Invoice.model";
import {FormGroup} from "@angular/forms";
import {PriceType} from "./PriceType";

export class InvoiceItem {
  recid: number = 0;
  name: String = '';
  amount: number = 0;
  unit: String = "szt.";
  vat: number = 23;
  priceVat: number = 0;
  priceNet: number = 0;
  priceGross: number = 0;
  priceAmountNet: number = 0;
  priceAmountGross: number = 0;
  taxationReason: TaxationReason = TaxationReason.other_goods_and_services;
  deduct50: boolean = false;
  description: string = '';
  invoice: Invoice = new Invoice(null);

  constructor(data: any) {
    if(data!=null) {
      this.name = data.name;
      this.amount = +data.amount;
      this.unit = data.unit;
      this.vat = data.vat;
      this.priceVat = data.priceVat;
      this.priceNet = +data.priceNet;
      this.priceGross = data.priceGross;
      this.priceAmountNet = data.priceAmountNet;
      this.priceAmountGross = data.priceAmountGross;
      this.taxationReason = data.taxationReason;
      this.deduct50 = data.deduct50;
      this.description = data.description;
    }
  }

  setDataOnPurchaseItem(form: FormGroup, priceType: PriceType) {
    this.setDataOnItem(form, priceType);
    this.deduct50 = form.get('deduct50')?.value;
    this.taxationReason = form.get('taxationReason')?.value;
  }

  setDataOnPurchaseNoVatItem(form: FormGroup) {
    this.name = form.get('name')?.value;
    this.priceNet = form.get('price')?.value;
    this.amount = form.get('amount')?.value;
    this.unit = form.get('unit')?.value;
    this.taxationReason = form.get('taxationReason')?.value;
  }

  setDataOnSaleItem(form: FormGroup, priceType: PriceType) {
    this.setDataOnItem(form, priceType);
    this.description = form.get('description')?.value;
  }

  setDataOnItem(form: FormGroup, priceType: PriceType) {
    this.name = form.get('name')?.value;
    if (priceType == PriceType.net) {
      this.priceNet = form.get('price')?.value;
    } else {
      this.priceGross = form.get('price')?.value;
    }
    this.amount = form.get('amount')?.value;
    this.unit = form.get('unit')?.value;
    this.vat = form.get('vat')?.value;
  }
}
