export class Document {
  recid: number;
  name: string;
  file: Blob|null;

  constructor(documentRecid: number,blob: Blob|null, name: string) {
    this.recid = documentRecid;
    this.file = blob;
    this.name = name;
  }
}
