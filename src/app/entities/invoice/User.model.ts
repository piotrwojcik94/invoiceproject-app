import {TaxationForm} from "./TaxationForm.model";

export class User {
  recid: number = 0;
  login: String = '';
  password: String = '';
  isAdmin: boolean = false;
  nip: String = '';
  userStartDate: Date = new Date();
  taxationForms: TaxationForm[] = [];

  constructor(userLogin: String, userPassword: String, userNip: String) {
    this.login = userLogin;
    this.password = userPassword;
    this.nip = userNip;
  }
}
