export enum InvoiceType {
  sale = "sale",
  purchase = "purchase",
  no_vat = "no_vat"
}

export const InvoiceTypeToLabelMapping: Record<InvoiceType, String> = {
  [InvoiceType.sale]: "Sprzedaż",
  [InvoiceType.purchase]: "Zakup",
  [InvoiceType.no_vat]: "Bez VAT"
};
