export class MonthSummary {
  income: number = 0;
  expense: number = 0;
  profit: number = 0;
  tax: number = 0;
  healthContribution: number = 0;
  totalTax: number = 0;
  totalProfit: number = 0;
}
