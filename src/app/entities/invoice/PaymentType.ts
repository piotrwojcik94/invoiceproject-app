export enum PaymentType {
  cash = "cash",
  cash_on_delivery = "cash_on_delivery",
  credit_card = "credit_card",
  transfer = "transfer",
  other = "other"
}

export const PaymentTypeToLabelMapping: Record<PaymentType, String> = {
  [PaymentType.cash]: "Gotówka",
  [PaymentType.cash_on_delivery]: "Za pobraniem",
  [PaymentType.credit_card]: "Karta płatnicza",
  [PaymentType.transfer]: "Przelew",
  [PaymentType.other]: "Inna"
};
