export class InvoiceIncomeAndExpenses {
  recid: number = 0;
  description: string = '';
  valueGoodsAndServicesSold: number = 0.0;
  otherIncome: number = 0.0;
  totalIncome: number = 0.0;
  payInCash: number = 0.0;
  otherExpenses: number = 0.0;
  totalExpenses: number = 0.0;
  comments: string = '';
}
