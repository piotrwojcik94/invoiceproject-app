export class Contractor {
  recid: number = 0;
  name: string = '';
  nip: string = '';
  regon: string = '';
  street: string = '';
  houseNumber: string = '';
  flatNumber: string = '';
  zipCode: string = '';
  city: string = '';
  privatePerson: boolean = false;
  createDate: Date = new Date();

  constructor(data: any) {
    if(data != null) {
      this.recid = data.recid;
      this.name = data.name;
      this.nip = data.nip;
      this.regon = data.regon;
      this.street = data.street;
      this.houseNumber = data.houseNumber;
      this.flatNumber = data.flatNumber;
      this.zipCode = data.zipCode;
      this.city = data.city;
      this.createDate = data.createDate;
    }
  }

  getContractorData() {
    let contractor = 'Firma: ' + this.name +
      '\nAdres: ' + this.getContractorAddress();
      contractor += '\nNIP: ' + this.nip;
    return contractor;
  }

  getContractorAddress() {
    let address = this.street + ' ' + this.houseNumber;
    if(this.flatNumber != null)
      address += '/'+this.flatNumber;
    address +=', ' + this.zipCode + ' ' + this.city;
    return address;
  }
}
