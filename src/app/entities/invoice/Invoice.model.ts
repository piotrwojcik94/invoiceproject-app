import {Contractor} from "./Contractor.model";
import {PriceType} from "./PriceType";
import {PaymentType} from "./PaymentType";
import {User} from "./User.model";
import {InvoiceItem} from "./InvoiceItem.model";
import {InvoiceType} from "./InvoiceType";
import {KPiR} from "./KPiR.model";
import {InvoiceIncomeAndExpenses} from "./InvoiceIncomeAndExpenses.model";
import {Document} from "./Document.model";

export class Invoice {
  recid: number = 0;
  number: string = '';
  delivery: Date = new Date();
  issue: Date = new Date();
  posting: Date = new Date();
  priceType: PriceType = PriceType.net;
  received: Date = new Date();
  payment: Date = new Date();
  releasePlace: string = '';
  paymentDeadlineLikeOnContract:boolean = false;
  daysToPay: number = 7;
  comment: string = '';
  paymentType: PaymentType = PaymentType.cash;
  payed: boolean = false;
  user: User|null = new User('', '', '');
  invoiceItems: InvoiceItem[] = [];
  contractor: Contractor = new Contractor(null);
  invoiceType: InvoiceType = InvoiceType.purchase;
  net: number = 0.0;
  vat: number = 0.0;
  gross: number = 0.0;
  isCost: boolean = false;
  vatCost: number = 0;
  netCost: number = 100;
  invoiceCost: number = 0;
  kpir: KPiR = new KPiR(null, new Date());
  invoiceIncomeAndExpenses: InvoiceIncomeAndExpenses = new InvoiceIncomeAndExpenses();
  document: Document|null;

  constructor(data: any) {
    if (data != null) {
      this.recid = data.recid;
      this.number = data.number;
      this.delivery = data.delivery;
      this.issue = data.issue;
      this.posting = data.posting;
      this.priceType = data.priceType;
      this.received = data.received;
      this.payment = data.payment;
      this.releasePlace = data.releasePlace;
      this.paymentDeadlineLikeOnContract = data.paymentDeadlineLikeOnContract;
      this.daysToPay = data.daysToPay;
      this.comment = data.comment;
      this.paymentType = data.paymentType;
      this.payed = data.payed;
      this.invoiceItems = data.invoiceItems;
      this.contractor = new Contractor(data.contractor);
      this.user = null;
      this.invoiceType = data.invoiceType;
      this.net = data.net;
      this.vat = data.vat;
      this.gross = data.gross;
      this.isCost = data.isCost;
      this.vatCost = data.vatCost;
      this.netCost = data.netCost;
      this.kpir = data.kpir;
      this.invoiceIncomeAndExpenses = data.invoiceIncomeAndExpenses;
      if(data.document?.file != null) {
        this.document = new Document(data.document.recid, data.document.file, data.document.name);
      }
    }
  }
}


