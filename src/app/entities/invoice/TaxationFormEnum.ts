export enum TaxationFormEnum {
  general_rules = "general_rules",
  linearly = "linearly",
  lump_sum = "lump_sum"
}

export const TaxationFormToLabelMapping: Record<TaxationFormEnum, string> = {
  [TaxationFormEnum.general_rules]: "Zasady ogólne",
  [TaxationFormEnum.linearly]: "Liniowo",
  [TaxationFormEnum.lump_sum]: "Ryczałt"
};
