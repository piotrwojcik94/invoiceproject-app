import {SelectedDate} from "./SelectedDate.model";

export class SelectedDateFromDate extends SelectedDate {
  constructor(date: Date) {
    super();
    let d: Date = new Date(date);
    this.month = d.getMonth();
    this.year = d.getFullYear();
  }
}
