import {SelectedDate} from "./invoice/SelectedDate.model";

export class SelectedDateFromMonthAndYear extends SelectedDate {

  constructor(selectedMonth: number, selectedYear: number) {
    super();
    this.month = selectedMonth;
    this.year = selectedYear;
  }
}
