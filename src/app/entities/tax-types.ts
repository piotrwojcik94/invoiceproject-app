export enum TaxTypes {
  progressive = "progressive",
  flat = "flat",
  lump_sum = "lump_sum"
}

export const TaxTypeToLabelMapping: Record<TaxTypes, String> = {
  [TaxTypes.progressive]: "Zasady ogólne",
  [TaxTypes.flat]: "Liniówka",
  [TaxTypes.lump_sum]: "Ryczałt"
};
