import {FormControl} from "@angular/forms";
import {NIPValidator} from "./NIPValidator";


describe('NIPValidator', () => {
  it('should return NIP required if NIP is empty', () => {
    const result = NIPValidator.validateNIP(new FormControl(''));
    expect(result).toEqual({nip: 'NIP jest wymagany'});
  });

  it('should return NIP required if NIP is null', () => {
    const result = NIPValidator.validateNIP(new FormControl(null));
    expect(result).toEqual({nip: 'NIP jest wymagany'});
  });

  it('should return NIP lenght must contain 10 digits', () => {
    const result = NIPValidator.validateNIP(new FormControl('123456789'));
    expect(result).toEqual({nip: 'NIP musi zawierać 10 cyfr'});
  });

  it('should return NIP lenght must contain 10 digits', () => {
    const result = NIPValidator.validateNIP(new FormControl('12345678901'));
    expect(result).toEqual({nip: 'NIP musi zawierać 10 cyfr'});
  });

  it('should return NIP invalid for 1234567890', () => {
    const result = NIPValidator.validateNIP(new FormControl('1234567890'));
    expect(result).toEqual({nip: 'NIP nie jest poprawny'});
  });

  it('should return null for 7142050141', () => {
    const result = NIPValidator.validateNIP(new FormControl('7142050141'));
    expect(result).toEqual(null);
  });
});
