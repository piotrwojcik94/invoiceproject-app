import {FormControl, FormGroup} from "@angular/forms";
import {PasswordConfirmValidator} from "./PasswordConfirmValidator";

describe('PasswordConfirmValidator', () => {
  let passwordConfirm: FormControl;
  let password;
  let formGroup: FormGroup;

  beforeEach(() => {
    password = new FormControl('password');
    passwordConfirm = new FormControl('password');
    formGroup = new FormGroup({password: password, passwordConfirm: passwordConfirm});
  });

  it('should return null when password match', () => {
    const result = PasswordConfirmValidator.validatePasswordConfirm(formGroup);
    expect(result).toEqual(null);
  });

  it('should return error when passwords do not match', () => {
    passwordConfirm.setValue('password123')
    const result = PasswordConfirmValidator.validatePasswordConfirm(formGroup);
    expect(result).toEqual({passwordMismatch: 'Hasła nie pasują'});
  });
});
