import {FormControl} from "@angular/forms";
import {UserService} from "../services/user.service";
import {fakeAsync, TestBed} from "@angular/core/testing";
import {of} from "rxjs";
import {UniqueNIPValidator} from "./UniqueNIPValidator";

describe('UniqueNIPValidator', () => {
  let validator: UniqueNIPValidator;
  let userServiceSpy: jasmine.SpyObj<UserService>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('UserService', ['isNIPUnique']);

    TestBed.configureTestingModule({
      providers: [
        UniqueNIPValidator,
        { provide: UserService, useValue: spy }
      ]
    });

    validator = TestBed.inject(UniqueNIPValidator);
    userServiceSpy = TestBed.inject(UserService) as jasmine.SpyObj<UserService>;
  });

  it('should return null if NIP is unique', fakeAsync(() => {
    userServiceSpy.isNIPUnique.and.returnValue(of(true));
    const control = new FormControl('uniqueNIP');
    validator.validate(control).subscribe((result) => {
      expect(result).toBeNull();
    });
  }));

  it('should return error if NIP is not unique', fakeAsync(() => {
    userServiceSpy.isNIPUnique.and.returnValue(of(false));
    const control = new FormControl('notUniqueNIP');
    validator.validate(control).subscribe((result) => {
      expect(result).toEqual({nip: 'Użytkownik o podanym NIP juz istnieje'});
    });
  }));

});
