import {EmailValidator} from "./EmailValidator";
import {FormControl} from "@angular/forms";


describe('EmailValidator', () => {
  const emptyEmails = [{ email: '' }, { email: null }];
  emptyEmails.forEach(({ email}) => {
    it(`should return e-mail required if email is ${email}`, () => {
      const result = EmailValidator.validateEmail(new FormControl(email));
      expect(result).toEqual({email: 'Adres e-mail jest wymagany'});
    });
  });

  const invalidEmails = [{ email: 'asd@g.p' }, { email: 'asd@.pl' },
    { email: '@gm.pl' }, { email: 'asd@g.' },
    { email: 'asdgmail.com' }
  ];
  invalidEmails.forEach(({ email}) => {
    it(`should return e-mail invalid for email ${email}`, () => {
      const result = EmailValidator.validateEmail(new FormControl(email));
      expect(result).toEqual({email: 'Adres e-mail nie jest poprawny'});
    });
  });

  const validEmails = [
    { email: 'name@gmail.com' },
    { email: 'n@g.pl' },
    { email: 'n@gm.pl' }
  ];
  validEmails.forEach(({ email }) => {
    it(`should return null for email ${email}`, () => {
      const result = EmailValidator.validateEmail(new FormControl(email));
      expect(result).toEqual(null);
    });
  });
});
