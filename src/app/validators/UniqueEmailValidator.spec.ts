import {FormControl} from "@angular/forms";
import {UniqueEmailValidator} from "./UniqueEmailValidator";
import {UserService} from "../services/user.service";
import {fakeAsync, TestBed} from "@angular/core/testing";
import {of} from "rxjs";

describe('UniqueEmailValidator', () => {
  let validator: UniqueEmailValidator;
  let userServiceSpy: jasmine.SpyObj<UserService>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('UserService', ['isEmailUnique']);

    TestBed.configureTestingModule({
      providers: [
        UniqueEmailValidator,
        { provide: UserService, useValue: spy }
      ]
    });

    validator = TestBed.inject(UniqueEmailValidator);
    userServiceSpy = TestBed.inject(UserService) as jasmine.SpyObj<UserService>;
  });

  it('should return null if username is unique', fakeAsync(() => {
    userServiceSpy.isEmailUnique.and.returnValue(of(true));
    const control = new FormControl('uniqueEmail');
    validator.validate(control).subscribe((result) => {
      expect(result).toBeNull();
    });
  }));

  it('should return error if username is not unique', fakeAsync(() => {
    userServiceSpy.isEmailUnique.and.returnValue(of(false));
    const control = new FormControl('notUniqueEmail');
    validator.validate(control).subscribe((result) => {
      expect(result).toEqual({email: 'Podany adres e-mail jest zajęty'});
    });
  }));


});
