import {PasswordValidator} from "./PasswordValidator";
import {FormControl} from "@angular/forms";

describe('PasswordValidator', () => {

  it('should return null when password is valid', () => {
    const formControl = new FormControl('Abcdef123')
    const result = PasswordValidator.validatePassword(formControl);
    expect(result).toEqual(null);
  });

  it(`should return error 'Enter password'`, () => {
    const formControl = new FormControl('')
    const result = PasswordValidator.validatePassword(formControl);
    expect(result).toEqual({password: 'Wprowadz hasło'});
  });

  it(`should return error 'Password must be 8 characters'`, () => {
    const formControl = new FormControl('abcdefg')
    const result = PasswordValidator.validatePassword(formControl);
    expect(result).toEqual({password: 'Hasło musi składać się z 8 znaków'});
  });

  it(`should return error 'Password must contain at least 1 digit'`, () => {
    const formControl = new FormControl('abcdefghijk')
    const result = PasswordValidator.validatePassword(formControl);
    expect(result).toEqual({password: 'Hasło musi zawierać co najmniej 1 cyfrę'});
  });

  it(`should return error 'Password must contain at least 1 uppercase letter'`, () => {
    const formControl = new FormControl('abcdefghijk1')
    const result = PasswordValidator.validatePassword(formControl);
    expect(result).toEqual({password: 'Hasło musi zawierać co najmniej 1 dużą literę'});
  });

  it(`should return error 'Password must contain at least 1 lowercase letter'`, () => {
    const formControl = new FormControl('ABCDEFGH1')
    const result = PasswordValidator.validatePassword(formControl);
    expect(result).toEqual({password: 'Hasło musi zawierać co najmniej 1 małą literę'});
  });
});
