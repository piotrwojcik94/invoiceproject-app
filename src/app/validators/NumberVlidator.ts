import {AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({
  selector: '[NumberValidator]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: NumberValidator, multi: true}
  ]
})
export class NumberValidator implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }

  static validate(control: FormControl): ValidationErrors | null {
    let value = control.value;

    if (value == null || value.length == 0) {
      return {value: 'Wartość nie może być pusta !'};
    } else if (isNaN(Number(value))) {
      return {value: 'Wartość niepoprawna !'};
    } else if (value <= 0) {
      return {value: 'Wartość musi być większa niż 0 !'}
    }

    return null;
  }
}
