import {FormControl} from "@angular/forms";
import {NumberValidator} from "./NumberVlidator";


describe('NumberValidator', () => {
  const emptyNumbers = [{ number: '' }, { number: null }];
  emptyNumbers.forEach(({ number}) => {
    it('should return value required', () => {
      const result = NumberValidator.validate(new FormControl(number));
      expect(result).toEqual({value: 'Wartość nie może być pusta !'});
    });
  });

  const notANumbers = [{ number: 'ad1' }, { number: 'null' }];
  notANumbers.forEach(({ number}) => {
    it('should return invalid value', () => {
      const result = NumberValidator.validate(new FormControl(number));
      expect(result).toEqual({value: 'Wartość niepoprawna !'});
    });
  });

  const lessThan0 = [{ number: 0 }, { number: -1 }];
  lessThan0.forEach(({ number}) => {
    it('should return value must be greater than 0', () => {
      const result = NumberValidator.validate(new FormControl(number));
      expect(result).toEqual({value: 'Wartość musi być większa niż 0 !'});
    });
  });

  const correctValues = [{ number: 1 }, { number: 2 }];
  correctValues.forEach(({ number}) => {
    it('should return null for correct values', () => {
      const result = NumberValidator.validate(new FormControl(number));
      expect(result).toEqual(null);
    });
  });

});
