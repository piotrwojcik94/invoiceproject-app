import {AbstractControl, FormControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({
  selector: '[PasswordValidator]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: PasswordValidator, multi: true }
  ]
})
export class PasswordValidator implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }

  static validatePassword(control: FormControl): ValidationErrors | null {
    let password = control.value
    let passwordError;
    if (password.length == 0)
      return {password: 'Wprowadz hasło'};

    passwordError = PasswordValidator.validatePasswordValue(password);

    if (passwordError != null) {
      return {password: passwordError};
    }

    return null;
  }

  static validatePasswordValue(password: string) {
    if(!new RegExp("(?=.{8,})").test(password))
      return 'Hasło musi składać się z 8 znaków';
    if(!new RegExp("(?=.*[0-9])").test(password))
      return 'Hasło musi zawierać co najmniej 1 cyfrę';
    if(!new RegExp("(?=.*[A-Z])").test(password))
      return 'Hasło musi zawierać co najmniej 1 dużą literę';
    if(!new RegExp("(?=.*[a-z])").test(password))
      return 'Hasło musi zawierać co najmniej 1 małą literę';
    return null;
  }

}
