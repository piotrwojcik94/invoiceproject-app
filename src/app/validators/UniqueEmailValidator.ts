import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors} from "@angular/forms";
import {catchError, Observable, of} from 'rxjs';
import {UserService} from "../services/user.service";
import {map} from 'rxjs/operators';
import {Directive} from "@angular/core";

@Directive({
  selector: '[UniqueEmailValidator]',
  providers: [
    { provide: NG_ASYNC_VALIDATORS, useExisting: UniqueEmailValidator, multi: true }
  ]
})
export class UniqueEmailValidator implements AsyncValidator {

  constructor(private userService: UserService) {}
  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.userService.isEmailUnique(control.value).pipe(
      map(isUnique => (isUnique ? null : {email: 'Podany adres e-mail jest zajęty'})),
      catchError(() => of(null))
    );
  }


}
