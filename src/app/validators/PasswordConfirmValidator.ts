import {AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({
  selector: '[PasswordConfirmValidator]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: PasswordConfirmValidator, multi: true }
  ]
})
export class PasswordConfirmValidator implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }

  static validatePasswordConfirm(formGroup: FormGroup): ValidationErrors | null {
    const password = formGroup.get('password')?.value;
    const passwordConfirm = formGroup.get('passwordConfirm')?.value;
    return password === passwordConfirm ? null : {passwordMismatch: 'Hasła nie pasują'};
  }

}
