import {FormControl} from "@angular/forms";
import {NotEmptyValidator} from "./NotEmptyValidator";


describe('NotEmptyValidator', () => {
  it('should return required if value is empty', () => {
    const result = NotEmptyValidator.validateNotEmpty(new FormControl(''));
    expect(result).toEqual({value: 'Pole nie może być puste!'});
  });

  it('should return required if value is null', () => {
    const result = NotEmptyValidator.validateNotEmpty(new FormControl(null));
    expect(result).toEqual({value: 'Pole nie może być puste!'});
  });

  it('should return null if value is 1', () => {
    const result = NotEmptyValidator.validateNotEmpty(new FormControl(1));
    expect(result).toEqual(null);
  });

  it('should return null if value "1"', () => {
    const result = NotEmptyValidator.validateNotEmpty(new FormControl('1'));
    expect(result).toEqual(null);
  });

});
