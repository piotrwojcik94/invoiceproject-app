import {AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({
  selector: '[NotEmptyValidator]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: NotEmptyValidator, multi: true}
  ]
})
export class NotEmptyValidator implements Validator {

  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }

  static validateNotEmpty(control: FormControl): ValidationErrors | null {
    let value = control.value;

    if (value == null || value.length == 0) {
      return {value: 'Pole nie może być puste!'};
    }

    return null;
  }
}
