import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors} from "@angular/forms";
import {catchError, Observable, of} from "rxjs";
import {Directive} from "@angular/core";
import {map} from "rxjs/operators";
import {UserService} from "../services/user.service";

@Directive({
  selector: '[UniqueNIPValidator]',
  providers: [
    { provide: NG_ASYNC_VALIDATORS, useExisting: UniqueNIPValidator, multi: true }
  ]
})
export class UniqueNIPValidator implements AsyncValidator {

  constructor(private userService: UserService) {}

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.userService.isNIPUnique(control.value).pipe(
      map(isUnique => (isUnique ? null : {nip: 'Użytkownik o podanym NIP juz istnieje'})),
      catchError(() => of(null))
    );
  }

}
