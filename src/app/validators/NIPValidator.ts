import {AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({
  selector: '[NIPValidator]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: NIPValidator, multi: true}
  ]
})
export class NIPValidator implements Validator {

  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }

  static validateNIP(control: FormControl): ValidationErrors | null {
    let NIP = control.value;

    if (NIP == null || NIP.length == 0) {
      return {nip: 'NIP jest wymagany'};
    } else if (NIP.length != 10) {
      return {nip: 'NIP musi zawierać 10 cyfr'};
    }

    let weights: number[] = [6, 5, 7, 2, 3, 4, 5, 6, 7];
    let sum = 0;
    for (let i = 0; i < NIP.length - 1; i++) {
      sum += +NIP[i] * weights[i];
    }
    if (!((sum % 11) == +NIP[9])) {
      return {nip: 'NIP nie jest poprawny'};
    }

    return null;
  }

}
