import {AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({
  selector: '[EmailValidator]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: EmailValidator, multi: true }
  ]
})
export class EmailValidator implements Validator {

  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }

  static validateEmail(control: FormControl): ValidationErrors | null {
    let emailRegex = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$');
    let email = control.value;

    if (email == null || email.length == 0)
      return {email: 'Adres e-mail jest wymagany'};
    else if (!emailRegex.test(email))
      return {email: 'Adres e-mail nie jest poprawny'};

    return null;
  }

}
