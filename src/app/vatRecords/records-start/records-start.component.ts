import {Component, OnInit, ViewChild} from "@angular/core";
import {InvoicesSummary} from "../../entities/invoice/invoices-summary.model";
import {Invoice} from "../../entities/invoice/Invoice.model";
import {InvoiceService} from "../../services/invoice.service";
import {InvoiceType} from "../../entities/invoice/InvoiceType";
import {Title} from "@angular/platform-browser";
import {MonthNavigationComponent} from "../../navigation/month-navigation/month-navigation.component";
import {InvoiceCalculator} from "../../calculator/invoice-calculator/invoice-calculator";

@Component({
  selector: 'app-records-start',
  templateUrl: './records-start.component.html',
  styleUrls: ['./records-start.component.css']
})
export class RecordsStartComponent implements OnInit {

  purchaseInvoices: Invoice[] = [];
  saleInvoices: Invoice[] = [];

  saleSummary: InvoicesSummary = new InvoicesSummary();
  purchaseSummary: InvoicesSummary = new InvoicesSummary();

  columnsForRecords: String[] = ["Lp.", "Data VAT", "Numer dowodu", "Nazwa, NIP", "Adres", "Netto", "Brutto", "VAT"];
  columnsForPay: String[] = ["VAT należny", "VAT naliczony", "VAT do zapłaty"];

  @ViewChild('childComponent') monthNavigation: MonthNavigationComponent;

  constructor(private titleService: Title,
              private invoiceService: InvoiceService) {
    this.titleService.setTitle("Ewidencja VAT");
  }

  ngOnInit() {
  }

  getData() {
    this.resetPrizes();
    this.getInvoices();
  }

  getInvoices() {
    this.invoiceService.getInvoicesByDateAndType(this.monthNavigation.getSelectedDate(), [InvoiceType.sale, InvoiceType.purchase])
      ?.subscribe((data) => {
        for (let invoice of data) {
          if (invoice.invoiceType == InvoiceType.purchase)
            this.purchaseInvoices.push(new Invoice(invoice));
          else
            this.saleInvoices.push(new Invoice(invoice));
        }
        this.updatePurchasePrizes();
        this.updateSalePrizes();
      });
  }

  updatePurchasePrizes() {
    InvoiceCalculator.calculateInvoicesSummary(this.purchaseInvoices, this.purchaseSummary);
  }

  updateSalePrizes() {
    InvoiceCalculator.calculateInvoicesSummary(this.saleInvoices, this.saleSummary);
  }

  resetPrizes() {
    this.purchaseInvoices = [];
    this.saleInvoices = [];
    this.saleSummary = new InvoicesSummary();
    this.purchaseSummary = new InvoicesSummary();
  }
}
