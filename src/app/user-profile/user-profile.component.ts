import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {User} from "../entities/invoice/User.model";
import {UserService} from "../services/user.service";
import {TaxationForm} from "../entities/invoice/TaxationForm.model";
import {TaxationFormEnum, TaxationFormToLabelMapping} from "../entities/invoice/TaxationFormEnum";
import {AppSettings} from "../app-settings";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user: User = new User('', '', '');
  taxationForms = TaxationFormToLabelMapping;
  taxationFormEnum = TaxationFormEnum;
  taxationKeys: TaxationFormEnum[];

  constructor(private titleService: Title,
              private userService: UserService) {
    this.titleService.setTitle("Profil użytkownika");
  }

  ngOnInit() {
    let nip = sessionStorage.getItem(AppSettings.USER_NIP);
    if (nip != null)
    this.userService.getUserByNIP(nip)?.subscribe(user => {
      this.user = user;
    });
  }

  onStartDateChange() {
    let taxationForms: TaxationForm[] = [];
    let startYear = new Date(this.user.userStartDate).getFullYear();
    let endYear = new Date().getFullYear();
    for(let i = startYear; i <= endYear; i++) {
      taxationForms.push(new TaxationForm(i));
    }
    this.user.taxationForms = taxationForms;
  }

  saveUser() {
    this.userService.updateUser(this.user)?.subscribe(response => {
      alert("Poprawnie zapisano profil użytkownika")
    });
  }

}
