import {AppSettings} from "./app-settings";
import {Injectable} from "@angular/core";
import {SelectedDate} from "./entities/invoice/SelectedDate.model";
import {SelectedDateFromMonthAndYear} from "./entities/SelectedDateFromMonthAndYear.model";

@Injectable()
export class BaseNavigationComponent {
  protected readonly currentDate: Date = new Date();
  protected readonly currentYear: number = this.currentDate.getFullYear();
  protected readonly currentMonth: number = this.currentDate.getMonth() + 1;
  protected readonly MONTHS_IN_YEAR: number = 12;
  yearOptions: number[] = [];
  selectedDate: SelectedDate = new SelectedDateFromMonthAndYear(this.currentMonth, this.currentYear);
  createdEconomicDate: SelectedDate;

  constructor() {
    this.setCreateDate();
  }

  setCreateDate() {
    let date = new Date(JSON.parse(<string>localStorage.getItem(AppSettings.CONTRACTOR)).createDate);
    this.createdEconomicDate = new SelectedDateFromMonthAndYear(date.getMonth() + 1, date.getFullYear());
  }

  getOptions(startNumber: number, endNumber: number) {
    let options: number[] = [];
    for (let i = startNumber; i <= endNumber; i++) {
      options.push(i);
    }
    return options;
  }

  public toString(): string {
    return '';
  }
}
