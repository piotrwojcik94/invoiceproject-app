import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import {Title} from "@angular/platform-browser";

describe('HomeComponent', () => {
  let titleService: Title;
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have title as 'Rozlicz fakturę'`, () => {
    titleService = TestBed.get(Title);
    expect(titleService.getTitle()).toBe('Rozlicz fakturę');
  });
});
