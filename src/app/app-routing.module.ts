import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SaleInvoiceListComponent} from "./invoice/invoiceList/saleInvoiceList/sale-invoice-list.component";
import {SaleInvoiceEditComponent} from "./invoice/invoiceEdit/saleInvoiceEdit/sale-invoice-edit.component";
import {PurchaseInvoiceListComponent} from "./invoice/invoiceList/purchaseInvoiceList/purchase-invoice-list.component";
import {PurchaseInvoiceEditComponent} from "./invoice/invoiceEdit/purchaseInvoiceEdit/purchase-invoice-edit.component";
import {RecordsComponent} from "./vatRecords/records/records.component";
import {RecordsStartComponent} from "./vatRecords/records-start/records-start.component";
import {RegisterUserComponent} from "./registerUser/register-user.component";
import {LoginUserComponent} from "./loginUser/login-user.component";
import {AuthGuardServiceService} from "./guard/auth-guard-service.service";
import {CanActivateContractor} from "./guard/contractor-guard";
import {KpirComponent} from "./kpir/kpir.component";
import {StatisticsComponent} from "./statistics/statistics.component";
import {HomeComponent} from "./home/home.component";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {BaseInvoiceEdit} from "./invoice/invoiceEdit/baseInvoiceEdit/base-invoice-edit";
import {ContractorListComponent} from "./contractor/contractor-list/contractor-list.component";
import {ContractorEditComponent} from "./contractor/contractor-edit/contractor-edit.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'sales', canActivate: [AuthGuardServiceService], component: BaseInvoiceEdit, children: [
      {path: '', component: SaleInvoiceListComponent},
      {path: 'new', component: SaleInvoiceEditComponent},
      {path: ':id/edit', component: SaleInvoiceEditComponent}
    ]
  },
  {
    path: 'purchases', canActivate: [AuthGuardServiceService], component: BaseInvoiceEdit, children: [
      {path: '', component: PurchaseInvoiceListComponent},
      {path: 'new', component: PurchaseInvoiceEditComponent},
      {path: ':id/edit', component: PurchaseInvoiceEditComponent}
    ]
  },
  {
    path: 'vatRecords', canActivate: [AuthGuardServiceService], component: RecordsComponent, children: [
      {path: '', component: RecordsStartComponent}
    ]
  },
  {
    path: 'contractors', canActivate: [AuthGuardServiceService], title: 'Lista kontraktorów', children: [
      {path: '', component: ContractorListComponent},
      {path: 'edit', component: ContractorEditComponent, title: 'Edycja kontraktora'}
    ]
  },
  {
    path: 'kpir', canActivate: [AuthGuardServiceService], component: KpirComponent
  },
  {
    path: 'statistics', canActivate: [AuthGuardServiceService], component: StatisticsComponent
  },
  {
    path: 'registerUser', component: RegisterUserComponent
  },
  {
    path: 'login', component: LoginUserComponent
  },
  {
    path: 'userProfile', canActivate: [AuthGuardServiceService], component: UserProfileComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
