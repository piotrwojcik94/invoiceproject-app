import {CanActivate, Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {AuthenticationService} from "../services/AuthenticationService.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuardServiceService implements CanActivate{

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canActivate(): boolean {
    if (this.authenticationService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}
