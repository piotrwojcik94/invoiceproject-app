import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import { AppComponent } from './app.component';
import {HeaderComponent} from "./header/header.component";
import {MockComponent} from "ng-mocks";
import {RouterModule} from "@angular/router";
import {Title} from "@angular/platform-browser";

describe('AppComponent', () => {
  let titleService: Title;
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule],
      declarations: [AppComponent, MockComponent(HeaderComponent)]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have title as 'Rozlicz Fakturę'`, () => {
    titleService = TestBed.get(Title);
    expect(titleService.getTitle()).toBe('Rozlicz fakturę');
  });

});
