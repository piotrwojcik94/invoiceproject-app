export class Statements {
  public static readonly QUESTION_ABOUT_REMOVE_INVOICE = 'Czy na pewno chcesz usunąć fakturę?';
  public static readonly CANNOT_REMOVE_INVOICE = 'Nie można skasować faktury, ponieważ jest wpisana do księgi przychodów i rozchodów.';
  public static readonly INVOICE_IN_READONLY_MODE = 'Faktura zostanie wyświetlona w trybie tylko do odczytu.';
}
