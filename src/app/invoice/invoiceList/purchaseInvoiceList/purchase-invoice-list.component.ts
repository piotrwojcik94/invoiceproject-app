import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InvoiceService} from "../../../services/invoice.service";
import {InvoiceType} from "../../../entities/invoice/InvoiceType";
import {Title} from "@angular/platform-browser";
import {BaseInvoiceList} from "../baseInvoiceList/base-invoice-list.component";
import {UploadFileService} from "../../../services/uploadService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-purchase-start',
  templateUrl: './purchase-invoice-list.component.html',
  styleUrls: ['./purchase-invoice-list.component.css']
})
export class PurchaseInvoiceListComponent extends BaseInvoiceList {
  invoiceType: InvoiceType[] = [InvoiceType.no_vat, InvoiceType.purchase];
  constructor(private titleService: Title,
              documentService: UploadFileService,
              invoiceService: InvoiceService,
              router: Router,
              route: ActivatedRoute,
              modalService: NgbModal) {
    super(invoiceService, router, route, documentService, modalService);
    this.titleService.setTitle("Lista faktur zakupu");
  }
}
