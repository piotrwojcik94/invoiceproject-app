import {InvoicesSummary} from "../../../entities/invoice/invoices-summary.model";
import {Invoice} from "../../../entities/invoice/Invoice.model";
import {InvoiceType, InvoiceTypeToLabelMapping} from "../../../entities/invoice/InvoiceType";
import {Subscription} from "rxjs";
import {Component, Input, OnDestroy, ViewChild} from "@angular/core";
import {InvoiceService} from "../../../services/invoice.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DocumentOpener} from "../../../tools/DocumentOpener";
import {UploadFileService} from "../../../services/uploadService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {MonthNavigationComponent} from "../../../navigation/month-navigation/month-navigation.component";
import {InvoiceCalculator} from "../../../calculator/invoice-calculator/invoice-calculator";
import {Statements} from "../../../statements";

@Component({
    selector: 'base-invoice-list',
    templateUrl: './base-invoice-list.html',
    styleUrls: ['./base-invoice-list.css']}
)
export class BaseInvoiceList implements OnDestroy {
  invoiceSummary: InvoicesSummary = new InvoicesSummary();
  invoices: Invoice[] = new Array<Invoice>();
  invoiceTypeMapping = InvoiceTypeToLabelMapping;
  subscription: Subscription;
  columns : String[] = ["Lp.", "Data Księgowania", "Nr faktury", "Typ faktury", "Nazwa kontrahenta", "NIP", "Netto [PLN]", "VAT [PLN]", "Brutto [PLN]", "Więcej"];

  @ViewChild('childComponent') monthNavigation: MonthNavigationComponent;

  @Input()
  invoiceTypes: InvoiceType[];

  constructor(private invoiceService: InvoiceService,
                        private router: Router,
                        private route: ActivatedRoute,
                        private documentService: UploadFileService,
                        private modalService: NgbModal) {
    this.subscription = this.invoiceService.subject.subscribe();
  }

  getData() {
    this.resetPrizes();
    this.getInvoices();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getInvoices(): void {
    this.invoiceService.getInvoicesByDateAndType(this.monthNavigation.getSelectedDate(), this.invoiceTypes)
      ?.subscribe((data) => {
        this.invoices = data;
        this.updateAllPrices();
      });
  }

  updateAllPrices() {
    InvoiceCalculator.calculateInvoicesSummary(this.invoices, this.invoiceSummary);
  }

  resetPrizes() {
    this.invoiceSummary = new InvoicesSummary();
  }

  newInvoice(invoiceType: string) {
    localStorage.setItem('invoiceType', JSON.stringify(invoiceType));
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  editInvoice(i : number) {
    let invoice : Invoice = this.invoices[i];
    if(invoice.isCost) {
      localStorage.setItem('readOnly', JSON.stringify(true));
      alert(Statements.INVOICE_IN_READONLY_MODE);
    }
    localStorage.setItem('invoiceType', JSON.stringify(invoice.invoiceType));
    localStorage.setItem('invoice', JSON.stringify(invoice));
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  deleteInvoice(i : number) {
    let invoice : Invoice = this.invoices[i];
    if(invoice.isCost) {
      alert(Statements.CANNOT_REMOVE_INVOICE);
    } else if(confirm(Statements.QUESTION_ABOUT_REMOVE_INVOICE)) {
      this.invoiceService.deleteInvoice(invoice)
        ?.subscribe( data => this.invoices = this.invoices.splice(i, 1));
      InvoiceCalculator.calculateInvoicesSummaryByRemoveInvoice(invoice, this.invoiceSummary);
    }
  }

  viewPdf(i: number) {
    if (this.invoices[i].document != null) {
      //@ts-ignore
      let recid = this.invoices[i].document.recid;
      new DocumentOpener(this.documentService, this.modalService).getDocumentAndOpen(recid);
    }
  }
}
