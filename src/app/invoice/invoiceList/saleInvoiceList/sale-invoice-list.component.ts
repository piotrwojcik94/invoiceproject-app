import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InvoiceService} from "../../../services/invoice.service";
import {InvoiceType} from "../../../entities/invoice/InvoiceType";
import {UploadFileService} from "../../../services/uploadService";
import {Title} from "@angular/platform-browser";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {BaseInvoiceList} from "../baseInvoiceList/base-invoice-list.component";

@Component({
  selector: 'app-sale-start',
  templateUrl: './sale-invoice-list.component.html',
  styleUrls: ['./sale-invoice-list.component.css']
})
export class SaleInvoiceListComponent extends BaseInvoiceList {
  invoiceType: InvoiceType[] = [InvoiceType.sale];
  constructor(private titleService: Title,
              documentService: UploadFileService,
              invoiceService: InvoiceService,
              router: Router,
              route: ActivatedRoute,
              modalService: NgbModal) {
    super(invoiceService, router, route, documentService, modalService);
    this.titleService.setTitle("Lista faktur sprzedaży");
  }
}
