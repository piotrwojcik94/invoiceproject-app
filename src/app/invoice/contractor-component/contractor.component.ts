import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {NIPValidator} from "../../validators/NIPValidator";
import {Contractor} from "../../entities/invoice/Contractor.model";
import {catchError, debounceTime, of, switchMap} from "rxjs";
import {GenerateContractorFromCeidgComponent} from "../../tools/GenerateContractorFromCeidg.component";
import {ContractorService} from "../../services/contractor.service";

@Component({
  selector: 'app-contractor-component',
  templateUrl: './contractor.component.html',
  styleUrl: './contractor.component.css'
})
export class ContractorComponent implements OnInit {
  contractorForm: FormGroup;
  readonly : boolean = false;
  contractorSuggestions: Contractor[] = [];

  @Input() contractor: Contractor;

  constructor(private fb: FormBuilder,
              private contractorService: ContractorService,) {
    this.contractorForm = fb.group({
      recid: [null],
      name: ['', Validators.required],
      nip: ['', [NIPValidator.validateNIP as ValidatorFn]],
      street: ['', Validators.required],
      houseNumber: ['', Validators.required],
      flatNumber: [null],
      zipCode: ['', Validators.required],
      city: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.contractor) {
      this.contractorForm.patchValue({ ...this.contractor });
    }
  }

  fillContractorFields(contractor: Contractor) {
    this.contractorForm.patchValue({ ...contractor });
    this.contractorSuggestions = [];
  }

  onContractorInputChange(): void {
    const nip = this.contractorForm.get('nip')?.value;

    if (nip?.length >= 3) {
      this.contractorService.getSuggestions(nip).pipe(
        debounceTime(300),
        switchMap(suggestions => of(suggestions)),
        catchError(() => of([]))
      ).subscribe(suggestions => {
        this.contractorSuggestions = suggestions;

        if (this.contractorSuggestions.length === 0 && this.contractorForm.get('nip')?.valid) {
          this.contractorService.getContractorFromCeidg(nip)
            .then(data => {
              const contractor = GenerateContractorFromCeidgComponent.generate(data?.result.subject);
              this.fillContractorFields(contractor);
            });
        }
      });
    } else {
      this.contractorSuggestions = [];
    }
  }
}
