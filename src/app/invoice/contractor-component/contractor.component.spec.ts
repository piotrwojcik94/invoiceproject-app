import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorComponent } from './contractor.component';
import {ContractorService} from "../../services/contractor.service";
import {FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MatInputModule} from "@angular/material/input";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {Contractor} from "../../entities/invoice/Contractor.model";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {of} from "rxjs";

describe('ContractorComponentComponent', () => {
  let component: ContractorComponent;
  let fixture: ComponentFixture<ContractorComponent>;
  let contractorService: ContractorService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContractorComponent ],
      imports: [ ReactiveFormsModule , HttpClientTestingModule, MatInputModule, MatAutocompleteModule, BrowserAnimationsModule ],
      providers: [ FormBuilder, ContractorService ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ContractorComponent);
    component = fixture.componentInstance;
    contractorService = TestBed.inject(ContractorService);
  });

  it('should create the component and initialize the form', () => {
    expect(component).toBeTruthy();
    expect(component.contractorForm).toBeDefined();
  });

  it('should patch contractor values on ngOnInit', () => {
    const contractor = new Contractor({
      recid: 1,
      name: 'Test Contractor',
      nip: '1234567890',
      street: 'Test Street',
      houseNumber: '10',
      flatNumber: '2',
      zipCode: '00-000',
      city: 'Test City'
    });
    component.contractor = contractor;
    fixture.detectChanges();

    expect(new Contractor(component.contractorForm.value)).toEqual(contractor);
  });

  it('should fill contractor fields with given contractor data', () => {
    const contractor = new Contractor({
      recid: 1,
      name: 'Test Contractor',
      nip: '1234567890',
      street: 'Test Street',
      houseNumber: '10',
      flatNumber: '2',
      zipCode: '00-000',
      city: 'Test City'
    });

    component.fillContractorFields(contractor);

    expect(new Contractor(component.contractorForm.value)).toEqual(contractor);
    expect(component.contractorSuggestions).toEqual([]);
  });

  it('should fetch contractor suggestions when NIP length is greater than or equal to 3', () => {
    const nip = '123';
    const mockSuggestions = [
      new Contractor({ recid: 1, name: 'Contractor 1', nip: '1234567890' }),
      new Contractor({ recid: 2, name: 'Contractor 2', nip: '1234567891' })
    ];

    spyOn(contractorService, 'getSuggestions').and.returnValue(of(mockSuggestions));

    component.contractorForm.get('nip')?.setValue(nip);
    fixture.detectChanges();

    component.onContractorInputChange();

    expect(contractorService.getSuggestions).toHaveBeenCalledWith(nip);
    expect(component.contractorSuggestions).toEqual(mockSuggestions);
  });

  it('should fetch contractor from CEIDG when no suggestions are found and NIP length is 10', async () => {
    const nip = '7142050141';
    const mockContractor = {
      result: {
        subject: {
          name: 'PIOTR WOJCIK',
          nip: nip,
          regon: '12121212',
          residenceAddress: null,
          workingAddress: 'Braci Wieniawskich 1/56, 20-844 Lublin',
          registrationLegalDate: new Date(),
          result: ''
        }
      }
    }
    const contractor = new Contractor({
      recid: 0,
      name: 'PIOTR WOJCIK',
      nip: '7142050141',
      houseNumber: '1',
      flatNumber: '56',
      street: 'Braci Wieniawskich ',
      city: 'Lublin ',
      zipCode: '20-844'
    })

    spyOn(contractorService, 'getSuggestions').and.returnValue(of([]));
    spyOn(contractorService, 'getContractorFromCeidg').and.returnValue(Promise.resolve(mockContractor));

    component.contractorForm.get('nip')?.setValue(nip);
    fixture.detectChanges();

    await component.onContractorInputChange();

    expect(contractorService.getContractorFromCeidg).toHaveBeenCalledWith(nip);
    expect(new Contractor(component.contractorForm.value)).toEqual(contractor);
  });

  it('should mark the form as invalid if required fields are empty', () => {
    component.contractorForm.controls['name'].setValue('');
    component.contractorForm.controls['nip'].setValue('');
    component.contractorForm.controls['street'].setValue('');
    component.contractorForm.controls['houseNumber'].setValue('');
    component.contractorForm.controls['zipCode'].setValue('');
    component.contractorForm.controls['city'].setValue('');
    fixture.detectChanges();

    expect(component.contractorForm.valid).toBeFalsy();
  });

  it('should mark the form as invalid if nip is invalid', () => {
    component.contractorForm.controls['name'].setValue('Contractor Name');
    component.contractorForm.controls['nip'].setValue('1234567890');
    component.contractorForm.controls['street'].setValue('Street Name');
    component.contractorForm.controls['houseNumber'].setValue('1');
    component.contractorForm.controls['zipCode'].setValue('00-000');
    component.contractorForm.controls['city'].setValue('City Name');
    fixture.detectChanges();

    expect(component.contractorForm.valid).toBeFalsy();
  });

  it('should mark the form as valid if all required fields are filled', () => {
    component.contractorForm.controls['name'].setValue('Contractor Name');
    component.contractorForm.controls['nip'].setValue('7142050141');
    component.contractorForm.controls['street'].setValue('Street Name');
    component.contractorForm.controls['houseNumber'].setValue('1');
    component.contractorForm.controls['zipCode'].setValue('00-000');
    component.contractorForm.controls['city'].setValue('City Name');

    expect(component.contractorForm.valid).toBeTruthy();
  });

  it('should fill the form with different contractor data', () => {
    const contractor = new Contractor({
      recid: 2,
      name: 'Another Contractor',
      nip: '9876543210',
      street: 'Another Street',
      houseNumber: '20',
      flatNumber: '3',
      zipCode: '11-222',
      city: 'Another City'
    });

    component.fillContractorFields(contractor);

    expect(new Contractor(component.contractorForm.value)).toEqual(contractor);
  });

  it('should update contractor form value when NIP is changed', () => {
    component.contractorForm.controls['nip'].setValue('1234567890');
    expect(component.contractorForm.controls['nip'].value).toBe('1234567890');
  });
});
