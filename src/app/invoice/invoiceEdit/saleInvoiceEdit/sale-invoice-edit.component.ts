import {Component, OnInit} from '@angular/core';
import {ContractorService} from "../../../services/contractor.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";
import {CalculateDateComponent} from "../../../tools/calculate-date.component";
import {PaymentType, PaymentTypeToLabelMapping} from "../../../entities/invoice/PaymentType";
import {InvoiceService} from "../../../services/invoice.service";
import pdfMake from 'pdfmake/build/pdfmake.js';
import pdfFonts from 'pdfmake/build/vfs_fonts.js';
import {UploadFileService} from "../../../services/uploadService";
import {Document} from "../../../entities/invoice/Document.model";
import {DocumentOpener} from "../../../tools/DocumentOpener";
import {InvoicePdfGenerator} from "../../../pdf/invoicePdf/InvoicePdfGenerator";
import {Title} from "@angular/platform-browser";
import {BaseInvoiceEdit} from "../baseInvoiceEdit/base-invoice-edit";
import {InvoiceItem} from "../../../entities/invoice/InvoiceItem.model";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-sale-edit',
  templateUrl: './sale-invoice-edit.component.html',
  styleUrls: ['./sale-invoice-edit.component.css']
})
export class SaleInvoiceEditComponent extends BaseInvoiceEdit implements OnInit {
  invoiceItem: InvoiceItem = new InvoiceItem(null);
  paymentTypesMapping = PaymentTypeToLabelMapping;
  paymentTypes = PaymentType;
  paymentTypesKeys: PaymentType[];

  removeDocument: boolean = false;

  uploadPdf: boolean = false;
  isPdfLoadedOnGui: boolean = false;

  columns: String[] = ["Nazwa", "Opis", "Cena jedn. netto", "Ilość", "J.M.", "Stawka VAT [%]", "NETTO [PLN]", "VAT [PLN]", "BRUTTO [PLN]"];

  constructor(private titleService: Title,
              private uploadService: UploadFileService,
              contractorService: ContractorService,
              invoiceService: InvoiceService,
              modalService: NgbModal,
              route: ActivatedRoute,
              router: Router) {
    super(contractorService, invoiceService, modalService, route, router);
    this.paymentTypesKeys = Object.values(this.paymentTypes);
    this.titleService.setTitle("Faktura sprzedaży");
  }

  override ngOnInit() {
    super.ngOnInit();
    this.initializeDates(new Date());
    this.changePriceTypeColumn();
  }

  initializeDates(date: Date) {
    this.invoice.issue = date;
    this.invoice.posting = date;
    this.invoice.delivery = date;
    let tempDate: Date = new Date(date);
    this.invoice.payment = new Date(tempDate.setDate(tempDate.getDate() + this.invoice.daysToPay));
  }

  changePriceTypeColumn() {
    if(this.invoice.priceType == this.priceTypeNet) {
      this.columns[2] = "Cena jedn. netto";
    } else {
      this.columns[2] = "Cena jedn. brutto";
    }
  }

  daysToPayChanged() {
    let tempDate: Date = new Date(this.invoice.issue);
    this.invoice.payment = new Date(tempDate.setDate(tempDate.getDate() + this.invoice.daysToPay));
  }

  changeDaysToPayment() {
    this.invoice.daysToPay = CalculateDateComponent.calculateDaysBetween2Dates(new Date(this.invoice.payment), new Date(this.invoice.issue));
  }

  displayFieldToUploadPDF() {
    this.uploadPdf = true;
  }

  generatePDF() {
    let invoicePdfGenerator = new InvoicePdfGenerator();
    invoicePdfGenerator.generate(this.invoice);
    this.isPdfLoadedOnGui = true;
  }

  selectFile(event: any) {
    this.invoice.document = new Document(0, event.target.files[0], 'name');
    this.isPdfLoadedOnGui = true;
  }

  deletePdf() {
    if(this.invoice.document?.file != null) {
      this.invoice.document = null;
    }
    this.uploadPdf = false;
    this.removeDocument = true;
  }

  openPdf() {
    let document = this.invoice.document;
    if (document != null) {
      let opener = new DocumentOpener(this.uploadService, this.modalService);
      if (this.isPdfLoadedOnGui) {
        opener.openDocument(document.file);
      } else {
        opener.getDocumentAndOpen(document.recid)
      }
    }
  }
}
