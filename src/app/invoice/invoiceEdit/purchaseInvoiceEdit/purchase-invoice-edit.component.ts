import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContractorService} from "../../../services/contractor.service";
import {TaxationReasonToLabelMapping} from "../../../entities/invoice/TaxationReason";
import {InvoiceService} from "../../../services/invoice.service";
import {Title} from "@angular/platform-browser";
import {BaseInvoiceEdit} from "../baseInvoiceEdit/base-invoice-edit";

@Component({
  selector: "app-purchase-edit",
  templateUrl: './purchase-invoice-edit.component.html',
  styleUrls: ['./purchase-invoice-edit.component.css']
})
export class PurchaseInvoiceEditComponent extends BaseInvoiceEdit implements OnInit {
  taxationReasonMapping = TaxationReasonToLabelMapping;

  columns: String[] = ["Nazwa", "Cena jedn. netto", "Ilość", "J.M.", "Stawka VAT [%]", "NETTO [PLN]", "VAT [PLN]", "BRUTTO [PLN]", "Odlicz 50%", "Powód opodatkowania"];
  noVatColumns: String[] = ["Nazwa", "Cena jedn. netto", "Ilość", "J.M.", "NETTO [PLN]", "Powód opodatkowania"];

  constructor(private titleService: Title,
              contractorService: ContractorService,
              invoiceService: InvoiceService,
              modalService: NgbModal,
              route: ActivatedRoute,
              router: Router) {
    super(contractorService, invoiceService, modalService, route, router);
    this.titleService.setTitle("Faktura zakupu");
  }

  override ngOnInit() {
    super.ngOnInit();
    this.initializeDates(new Date());
    this.changePriceTypeColumn();
  }

  initializeDates(date: Date) {
    this.invoice.issue = date;
    this.invoice.delivery = date;
    this.invoice.posting = date;
    this.invoice.received = date;
  }

  changePriceTypeColumn() {
    if (this.invoice.priceType == this.priceTypeNet) {
      this.columns[1] = "Cena jedn. netto";
    } else {
      this.columns[1] = "Cena jedn. brutto";
    }
  }
}
