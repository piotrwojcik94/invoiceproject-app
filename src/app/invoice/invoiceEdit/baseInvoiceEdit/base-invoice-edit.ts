import {Invoice} from "../../../entities/invoice/Invoice.model";
import {PriceType, PriceTypeToLabelMapping} from "../../../entities/invoice/PriceType";
import {InvoiceType} from "../../../entities/invoice/InvoiceType";
import {ContractorService} from "../../../services/contractor.service";
import {InvoiceService} from "../../../services/invoice.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";
import {Component, OnDestroy, ViewChild} from "@angular/core";
import {InvoiceItem} from "../../../entities/invoice/InvoiceItem.model";
import {PurchaseInvoiceItemComponent} from "../newInvoiceItem/purchaseInvoiceItem/purchase-invoice-item.component";
import {PurchaseNoVatInvoiceItemComponent} from "../newInvoiceItem/purchaseNoVatInvoiceItem/purchase-no-vat-invoice-item.component";
import {SaleInvoiceItemComponent} from "../newInvoiceItem/saleInvoiceItem/sale-invoice-item.component";
import {ContractorComponent} from "../../contractor-component/contractor.component";
import {NgForm} from "@angular/forms";
import {InvoiceItemCalculator} from "../../../calculator/invoice-item-calculator/invoice-item-calculator";

@Component({selector: 'base-invoice-edit',
  templateUrl: './base-invoice-edit.html',
  styleUrls: ['./base-invoice-edit.css']})
export class BaseInvoiceEdit implements OnDestroy {
  invoiceType: InvoiceType;
  invoice: Invoice = new Invoice(null);

  priceTypesMapping = PriceTypeToLabelMapping;
  priceTypes = PriceType;
  priceTypesKeys: PriceType[];
  priceTypeNet: PriceType = PriceType.net;

  @ViewChild('invoiceForm') invoiceForm: NgForm;

  readonly: boolean = false;

  selectedInvoiceItem: number = -1;

  @ViewChild(ContractorComponent) contractorComponent: ContractorComponent;

  constructor(private contractorService: ContractorService,
              private invoiceService: InvoiceService,
              protected modalService: NgbModal,
              private route: ActivatedRoute,
              private router: Router) {
    this.priceTypesKeys = Object.values(this.priceTypes);
  }

  ngOnInit() {
    let invoice = JSON.parse(<string>localStorage.getItem('invoice'));
    if(invoice != null) {
      this.invoice = new Invoice(invoice);
    }
    this.readonly = JSON.parse(<string>localStorage.getItem('readOnly'));
    this.setInvoiceType();
  }

  ngOnDestroy() {
    localStorage.removeItem('invoice');
    localStorage.removeItem('readOnly');
    localStorage.removeItem('invoiceType');
  }

  setInvoiceType() {
    let invoiceType = JSON.parse(<string>localStorage.getItem('invoiceType'));
    if (invoiceType == InvoiceType.no_vat) {
      this.invoiceType = InvoiceType.no_vat;
    } else if (invoiceType == InvoiceType.purchase) {
      this.invoiceType = InvoiceType.purchase;
    } else if(invoiceType == InvoiceType.sale) {
      this.invoiceType = InvoiceType.sale;
    }
  }

  addNewInvoiceItem() {
    this.openModal(new InvoiceItem(null), false);
  }

  editInvoiceItem() {
    this.openModal(new InvoiceItem(this.invoice.invoiceItems[this.selectedInvoiceItem]), true);
  }

  openModal(invoiceItem: InvoiceItem, editMode: boolean) {
    const modal = this.getModal();
    modal.componentInstance.invoiceItem = invoiceItem;
    modal.componentInstance.priceType = this.invoice.priceType;
    modal.componentInstance.emitData.subscribe(($event: any) => {
      let item: InvoiceItem = new InvoiceItem($event.item);
      InvoiceItemCalculator.calculateInvoiceAndItemPrices(this.invoice, item, editMode, this.selectedInvoiceItem);
    });
  }

  getModal() {
    if (this.invoiceType == InvoiceType.sale) {
      return this.modalService.open(SaleInvoiceItemComponent);
    } else if (this.invoiceType == InvoiceType.purchase) {
      return this.modalService.open(PurchaseInvoiceItemComponent);
    } else {
      return this.modalService.open(PurchaseNoVatInvoiceItemComponent);
    }
  }

  setSelectedRow(index: number) {
    if(index == this.selectedInvoiceItem) {
      this.selectedInvoiceItem = -1;
    } else {
      this.selectedInvoiceItem = index;
    }
  }

  invoiceItemsEmpty() {
    return this.invoice.invoiceItems.length != 0;
  }

  isInvoiceItemSelected() {
    return this.selectedInvoiceItem > -1;
  }

  removeInvoiceItem() {
    if(confirm("Czy na pewno chcesz usunąć pozycję?")) {
      InvoiceItemCalculator.calculateInvoiceSummaryByRemoveInvoiceItem(this.invoice, this.invoice.invoiceItems[this.selectedInvoiceItem]);
      this.invoice.invoiceItems.splice(this.selectedInvoiceItem,1);
      this.selectedInvoiceItem = -1;
    }
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  isFormInvalid() {
    return (this.invoiceForm && this.invoiceForm.invalid) || this.contractorComponent?.contractorForm?.invalid || this.invoice.invoiceItems.length == 0 || this.readonly;
  }

  onSubmit() {
    this.invoice.invoiceType = this.invoiceType;
    this.invoice.contractor = this.contractorComponent.contractorForm.value;
    this.invoiceService.saveInvoice(this.invoice)?.subscribe(
      (response) => {
        if (response?.status == 200) {
          this.router.navigate(['../'], {relativeTo: this.route})
        }
      }, error => console.log(error));
  }

}
