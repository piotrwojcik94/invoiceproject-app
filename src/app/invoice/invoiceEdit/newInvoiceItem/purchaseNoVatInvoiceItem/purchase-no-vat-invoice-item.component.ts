import {Component, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {SetDataOnInvoiceItemForm} from "../../../../tools/SetDataOnInvoiceItemForm";
import {BaseInvoiceItem} from "../baseInvoiceItem/base-invoice-item";
import {InvoiceItem} from "../../../../entities/invoice/InvoiceItem.model";

@Component({
  selector: 'app-new-purchase-no-vat-item',
  templateUrl: './purchase-no-vat-invoice-item.component.html',
  styleUrls: ['./purchase-no-vat-invoice-item.component.css']
})
export class PurchaseNoVatInvoiceItemComponent extends BaseInvoiceItem implements OnInit {
  invoiceItem: InvoiceItem = new InvoiceItem(null);

  constructor(activeModal: NgbActiveModal) {
    super(activeModal);
    this.itemForm = new FormGroup({
      name: this.name,
      price: this.price,
      amount: this.amount,
      unit: this.unit,
      taxationReason: this.taxationReason
    });
  }

  ngOnInit() {
    new SetDataOnInvoiceItemForm().setPurchaseNoVatItem(this.itemForm, this.invoiceItem);
  }

  onSubmit() {
    this.invoiceItem.setDataOnPurchaseNoVatItem(this.itemForm);
    this.emitData.emit({item: this.invoiceItem});
  }
}
