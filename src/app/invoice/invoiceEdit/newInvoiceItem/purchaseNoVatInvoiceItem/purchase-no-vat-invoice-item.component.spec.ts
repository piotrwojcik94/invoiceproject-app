import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseNoVatInvoiceItemComponent } from './purchase-no-vat-invoice-item.component';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

describe('NewPurchaseNoVatItemComponent', () => {
  let component: PurchaseNoVatInvoiceItemComponent;
  let fixture: ComponentFixture<PurchaseNoVatInvoiceItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [ PurchaseNoVatInvoiceItemComponent ],
      providers: [NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseNoVatInvoiceItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
