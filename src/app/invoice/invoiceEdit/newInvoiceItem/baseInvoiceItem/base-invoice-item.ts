import {FormControl, FormGroup, ValidatorFn} from "@angular/forms";
import {NotEmptyValidator} from "../../../../validators/NotEmptyValidator";
import {NumberValidator} from "../../../../validators/NumberVlidator";
import {EventEmitter, Injectable, Output} from "@angular/core";
import {PriceType} from "../../../../entities/invoice/PriceType";
import {TaxationReason, TaxationReasonToLabelMapping} from "../../../../entities/invoice/TaxationReason";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export abstract class BaseInvoiceItem {
  taxationReasonMapping = TaxationReasonToLabelMapping;
  taxationReasons = TaxationReason;
  taxationReasonKeys: TaxationReason[];

  itemForm: FormGroup;
  name: FormControl = new FormControl('', [NotEmptyValidator.validateNotEmpty as ValidatorFn]);
  price: FormControl = new FormControl('', [NumberValidator.validate as ValidatorFn]);
  amount: FormControl = new FormControl('', [NumberValidator.validate as ValidatorFn]);
  unit: FormControl = new FormControl('', [NotEmptyValidator.validateNotEmpty as ValidatorFn]);
  description: FormControl = new FormControl('', [NotEmptyValidator.validateNotEmpty as ValidatorFn]);
  vat: FormControl = new FormControl('', [NotEmptyValidator.validateNotEmpty as ValidatorFn]);
  deduct50: FormControl = new FormControl('');
  taxationReason: FormControl = new FormControl('');

  public priceType: PriceType = PriceType.net;
  priceTypes: PriceType = PriceType.net;

  @Output() emitData: EventEmitter<any> = new EventEmitter();

  protected constructor(protected activeModal: NgbActiveModal) {
    this.taxationReasonKeys = Object.values(TaxationReason);
  }

  abstract onSubmit(): void;

  onSubmitItem() {
    this.onSubmit();
    this.activeModal.close();
  }
}
