import {Component, OnInit} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {SetDataOnInvoiceItemForm} from "../../../../tools/SetDataOnInvoiceItemForm";
import {BaseInvoiceItem} from "../baseInvoiceItem/base-invoice-item";
import {InvoiceItem} from "../../../../entities/invoice/InvoiceItem.model";

@Component({
  selector: 'new-purchase-item',
  templateUrl: './purchase-invoice-item.component.html',
  styleUrls: ['./purchase-invoice-item.component.css']
})
export class PurchaseInvoiceItemComponent extends BaseInvoiceItem implements OnInit {
  invoiceItem: InvoiceItem = new InvoiceItem(null);

  constructor(activeModal: NgbActiveModal) {
    super(activeModal);
    this.itemForm = new FormGroup({
      name: this.name,
      price: this.price,
      amount: this.amount,
      unit: this.unit,
      vat: this.vat,
      deduct50: this.deduct50,
      taxationReason: this.taxationReason
    });
  }

  ngOnInit() {
    new SetDataOnInvoiceItemForm().setPurchaseItem(this.itemForm, this.invoiceItem, this.priceType);
  }

  onSubmit() {
    this.invoiceItem.setDataOnPurchaseItem(this.itemForm, this.priceType);
    this.emitData.emit({item: this.invoiceItem});
  }
}
