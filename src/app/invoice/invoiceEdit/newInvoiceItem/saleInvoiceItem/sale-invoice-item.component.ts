import {Component, OnInit} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {SetDataOnInvoiceItemForm} from "../../../../tools/SetDataOnInvoiceItemForm";
import {BaseInvoiceItem} from "../baseInvoiceItem/base-invoice-item";
import {InvoiceItem} from "../../../../entities/invoice/InvoiceItem.model";

@Component({
  selector: 'new-sale-item',
  templateUrl: './sale-invoice-item.component.html',
  styleUrls: ['./sale-invoice-item.component.css']
})
export class SaleInvoiceItemComponent extends BaseInvoiceItem implements OnInit {
  invoiceItem: InvoiceItem = new InvoiceItem(null);

  constructor(activeModal: NgbActiveModal) {
    super(activeModal);
    this.itemForm = new FormGroup({
      name: this.name,
      description: this.description,
      price: this.price,
      amount: this.amount,
      unit: this.unit,
      vat: this.vat
    });
  }

  ngOnInit() {
    new SetDataOnInvoiceItemForm().setSaleItem(this.itemForm, this.invoiceItem, this.priceType);
  }

  onSubmit() {
    this.invoiceItem.setDataOnSaleItem(this.itemForm, this.priceType);
    this.emitData.emit({item: this.invoiceItem});
  }
}
